const bcrypt = require("bcryptjs");
const messageResponse = require("./../utilities/MessageResponse");

module.exports = function(req, res, next){
    const userSession = req.header("I-API-CORE");
    if(!userSession){
        res.json(messageResponse().MISSING_HEADER_FIELD);
    }else{
        let salt = "D@i1y-c0R3-P4$$w0rK";
        // let lenghtCode = bcrypt.genSaltSync(10);
        // let code = bcrypt.hashSync(salt, lenghtCode);
        // $2a$10$6JmCf7GhoiLeSZWxC7uK2OjnMot1VYreLhaUOld5A8ue98572osVS
        let checkToken = bcrypt.compareSync(salt, userSession);
        if(checkToken){
            next();
        }else{
            res.json(messageResponse().WRONG_TOKEN_KEY);
        }
    }
}