module.exports = {
    "FORMAT_FULL_DATE": "DD-MM-YYYY HH:mm:ss",
    "FORMAT_DATE": "DD-MM-YYYY",
    "SALT": "u$3r_cR347E",
    "IS_TRUE": true,
    "IS_FALSE": false,
};