
const moment = require("moment");
const constants = require("./Constants");

module.exports = function (dataSearch){
    let query = {}, andQuery = { "$and": [] };
    let agencyName = dataSearch.agency_name;
    let dateStart = dataSearch.date_start;
    let dateEnd = dataSearch.date_end;
    let start_time, end_time;
    
    if(agencyName != null){
        agencyNameQuery = { "agency_name": new RegExp(agencyName, 'i') };
        andQuery.$and.push(agencyNameQuery);
        query = andQuery;
    }
    if(dateStart != null){
        let startTimeEpoch = moment(dateStart, constants.FORMAT_FULL_DATE).valueOf();
        start_time = {"time_create_epoch" : {$gte: startTimeEpoch}};
        andQuery.$and.push(start_time);
        query = andQuery;
    }
    if(dateEnd != null){
        let endTimeEpoch = moment(dateEnd, constants.FORMAT_FULL_DATE).valueOf();
        end_time = {"time_create_epoch" : {$lte: endTimeEpoch}};
        andQuery.$and.push(end_time);
        query = andQuery;
    }
    return query;
};