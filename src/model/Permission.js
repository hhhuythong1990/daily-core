const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const processSkipLimit = require("./../utilities/processSkipLimit");
const constants = require("./../utilities/Constants");

const PermissionSchema = mongoose.Schema({
    permission_name: {
        type: String,
        trim: true,
        unique: true,
        required:true    
    },
    permission_name_slug: {
        type: String,
        trim: true,
        required:true    
    },
    is_publish: {
        type: Boolean,
        required: true
    },
    day_create_date: {
        type: Date,
        required: true
    },
    day_update_date: {
        type: Date,
        default: null
    },
    day_create_string: {
        type: String,
        required: true
    },
    day_update_string: {
        type: String,
        default: null
    },
    user_create: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        required: true
    },
    user_update: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        default: null
    },
    temp: {
        type:  Object,
        default: null
    }
});

const Permission = module.exports = mongoose.model("Permission", PermissionSchema);

module.exports.insert = (newPermission, callback) => {
    newPermission.save(callback);
}

module.exports.takeAllPermission = (callback) => {
    Permission.find(callback);
}

module.exports.takeByDataSearchSkipLimit = (skipItem, limitItem, dataSearch, callback) => {
    let dataProcess = processSkipLimit(skipItem, limitItem);
    let limit = dataProcess.limit;
    let skip = dataProcess.skip;
    let query = {"permission_name" : new RegExp(dataSearch, 'i')};
    Permission.find(query)
        .populate({ "path" : "user_create", "select": "full_name" })
        .populate({ "path" : "user_update", "select": "full_name" })
        .skip(skip).limit(limit).exec(callback);
}

module.exports.takeBySkipLimit = (skipItem, limitItem, callback) => {
    let dataProcess = processSkipLimit(skipItem, limitItem);
    let limit = dataProcess.limit;
    let skip = dataProcess.skip;
    Permission.find()
        .populate({ "path" : "user_create", "select": "full_name" })
        .populate({ "path" : "user_update", "select": "full_name" })
        .skip(skip).limit(limit).exec(callback);
}

module.exports.sum = (callback) => {
    Permission.count(callback);
}

module.exports.sumQuery = (dataSearch, callback) => {
    let query = {"permission_name" : new RegExp(dataSearch, 'i')};
    Permission.find(query).count(callback);
}

module.exports.deleteById = (id, callback) => {
    Permission.remove({"_id": mongoose.Types.ObjectId(id)}, callback);
};

module.exports.updateById = (id, dataUpdate, callback) => {
    let query = {"_id": mongoose.Types.ObjectId(id)};
    Permission.update(query, {$set: dataUpdate}, { multi:true }, callback);
};

module.exports.takePermissionById = (id, callback) => {
    let query = {"_id": mongoose.Types.ObjectId(id)};
    Permission.findOne(query).exec(callback);
}

module.exports.checkPermissionByIdAndPermissionName = (id, permissionName, callback) => {
    let query = { $and: [{ "_id": { $ne: mongoose.Types.ObjectId(id) } },{ "permission_name" : permissionName }]};
    Permission.findOne(query).exec(callback);
}

module.exports.takePermissionByPermissionName = (permissionName, callback) => {
    let query = { "permission_name" : permissionName };
    Permission.findOne(query).exec(callback);
}

module.exports.getAllPermission = (callback) => {
    Permission.find().exec(callback);
}

module.exports.getPermissionShow = (callback) => {
    let query = {"is_publish" : constants.IS_TRUE}
    Permission.find(query).exec(callback);
}