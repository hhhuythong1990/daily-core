const moment = require("moment");
const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const processSkipLimit = require("./../utilities/processSkipLimit");
const processSearch = require("./../utilities/processSearch");
const constants = require("./../utilities/Constants");
require("./GiftCode");
require("./Profile");


const RequestGiftCodeSchema = mongoose.Schema({
    "user_create": { // object đại lý mua gói
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        required: true
    },
    use_form: { // hình thức mua
        type: String,
        default: null
    },
    "agency_name": { // tên đại lý
        type: String,
        required: true
    },
    "agency_package_name": { // tên gói
        type: String,
        required: true
    },
    "old_payment_limit": { // hạn mức trước khi mua gói
        type: Number,
        required: true
    },
    "price_paid": { // số tiền phải trả khi mua gói
        type: Number,
        required: true
    },
    "new_payment_limit": { // hạn mức mới
        type: Number,
        required: true
    },
    "amount_code_request": { // số lượng code yêu cầu tạo
        type: Number,
        required: true
    },
	"amount_code_created" : { // số lượng code nhận về từ payment
        type: Number,
        required: true
    },
    "price_original" : { // giá gốc của gói
        type: Number,
        required: true
    },
    "price_cycle" : { // giá chiết khấu
        type: Number,
        required: true
    },
    "price_after_cycle" : { // giá sau chiết khấu
        type: Number,
        required: true
    },
    "price_original_paid" : { // giá gốc phải trả
        type: Number,
        required: true
    },
    price_paid_difference: { // giá chênh lệch phải trả
        type: Number,
        required: true
    },
    price_difference: { // giá chênh lệch 1 đơn vị
        type: Number,
        required: true
    },
	"gift_code" :  [{ // id code tạo ra
        type:  mongoose.Schema.Types.ObjectId,
        ref: "GiftCode",
        required: true
    }],
    "transaction": { //Mã giao dịch
        type:  String,
        ref: "TransactionCode",
        required: true
    },
    //
    day_create_date: {
        type: Date,
        required: true
    },
    day_create_string: {
        type: String,
        required: true
    },
    time_create_epoch:{
        type: Number,
        required: true
    },
    temp: {
        type:  Object,
        default: null
    }
});

const RequestGiftCode = module.exports = mongoose.model("RequestGiftCode", RequestGiftCodeSchema);

module.exports.takeDataSearch = (dataSearch, callback) => {
    let query = processSearch(dataSearch);
    RequestGiftCode.find(query)
        .populate("gift_code")
        .populate("user_create")
        .sort({"time_create_epoch": -1})
        .exec(callback);
};

module.exports.takeDataSearchSkipLimit = (skipItem, limitItem, dataSearch, callback) => {
    let dataProcess = processSkipLimit(skipItem, limitItem);
    let limit = dataProcess.limit;
    let skip = dataProcess.skip;
    let query = processSearch(dataSearch);
    RequestGiftCode.find(query)
        .populate("gift_code")
        .populate("user_create")
        .sort({"time_create_epoch": -1})
        .skip(skip).limit(limit)
        .exec(callback);
};

module.exports.sumQuery = (dataSearch, callback) => {
    let query = processSearch(dataSearch);
    RequestGiftCode.find(query).count(callback);
}

module.exports.sumPaid = (dataSearch, callback) => {
    let query = processSearch(dataSearch);
    let group = { "_id": null  ,"totalPaid":{ $sum: "$price_paid"}, "totalDifference":{ $sum: "$price_paid_difference"}, "totalAmount":{ $sum: "$amount_code_created"} };
    RequestGiftCode.aggregate([{ $match: query }, { $group: group }], 
        function (err, result) {
        if (err) {
            callback(err, null);
        }else{
            callback(null, result);
        }
    });
}