const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const processSkipLimit = require("./../utilities/processSkipLimit");

const ModuleSchema = mongoose.Schema({
    module_name: {
        type: String,
        trim: true,
        required:true    
    },
    module_name_slug: {
        type: String,
        trim: true,
        required:true
    },
    route_path: {
        type: String,
        trim: true,
        required:true
    },
    is_publish: {
        type: Boolean,
        required: true
    },
    day_create_date: {
        type: Date,
        required: true
    },
    day_create_string: {
        type: String,
        required: true
    },
    day_update_date: {
        type: Date,
        default: null
    },
    day_update_string : {
        type: String,
        default: null
    },
    user_create: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        required: true
    },
    user_update: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        default: null
    },
    temp: {
        type:  Object,
        default: null
    }
});

const Module = module.exports = mongoose.model("Module", ModuleSchema);

module.exports.insertModule = (newModule, callback) => {
    newModule.save(callback);
}

module.exports.takeDataSearchSkipLimit = (skipItem, limitItem, dataSearch, callback) => {
    let dataProcess = processSkipLimit(skipItem, limitItem);
    let limit = dataProcess.limit;
    let skip = dataProcess.skip;
    let query = {"module_name" : new RegExp(dataSearch, 'i')};
    Module.find(query)
        .populate({ "path" : "user_create", "select": "full_name" })
        .populate({ "path" : "user_update", "select": "full_name" })
        .skip(skip).limit(limit).exec(callback);
};

module.exports.takeModuleBySkipLimit = (skipItem, limitItem, callback) => {
    let dataProcess = processSkipLimit(skipItem, limitItem);
    let limit = dataProcess.limit;
    let skip = dataProcess.skip;
    Module.find()
        .populate({ "path" : "user_create", "select": "full_name" })
        .populate({ "path" : "user_update", "select": "full_name" })
        .skip(skip).limit(limit).exec(callback);
};

module.exports.takeAllModule = (callback) => {
    Module.find(callback);
};

module.exports.sumModules = (callback) => {
    Module.count(callback);
}

module.exports.sumQuery = (dataSearch, callback) => {
    let query = {"module_name" : new RegExp(dataSearch, 'i')};
    Module.find(query).count(callback);
}

module.exports.takeModuleByName = (moduleName, callback) =>{
    const query = {"module_name": moduleName};
    Module.findOne(query, callback);
};

module.exports.checkModuleByModuleName = (moduleId, moduleName, callback) =>{
    const query ={$and:[{"_id":{$ne: mongoose.Types.ObjectId(moduleId)}},{"module_name": moduleName}]};
    Module.findOne(query, callback);
};

module.exports.updateModuleByModuleId = (moduleId, dataUpdate, callback) => {
    let query = {"_id": mongoose.Types.ObjectId(moduleId)};
    Module.update(query, {$set: dataUpdate}, { multi:true}, callback);
};

module.exports.deleteModuleByModuleId = (moduleId, callback) => {
    Module.remove({"_id": mongoose.Types.ObjectId(moduleId)}, callback);
};

module.exports.takeModuleByModuleId = (moduleId, callback) => {
    let query = {"_id": mongoose.Types.ObjectId(moduleId)};
    Module.findOne(query, callback);
};

module.exports.takeModuleByModuleName = (moduleName, callback) => {
    let query = { "module_name" : moduleName };
    Module.findOne(query).exec(callback);
}