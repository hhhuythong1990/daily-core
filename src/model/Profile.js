const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const processSkipLimit = require("./../utilities/processSkipLimit");

const ProfileSchema = mongoose.Schema({
    full_name: { // tên người đăng nhập
        type: String,
        trim: true,
        required:true
    }, 
    user_name: { 
        type: String,
        trim: true,
        unique: true,
        required: true
    },
    password: {
        type: String,
        trim: true
    },
    is_locked: {
        type: Boolean,
        default: false
    },
    is_publish: { 
        type: Boolean,
        required: true
    },
    permissions: [{
        type:  mongoose.Schema.Types.ObjectId,
        ref: "Permission",
        required: true    
    }],
    group_user: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: "GroupUser",
        // required: true
    },
    group_parent:[{ // dùng cho các đại lý con/ Admin
        type:  mongoose.Schema.Types.ObjectId,
        ref: "GroupUser",
        default: null    
    }],
    group_modules: [{
        type:  mongoose.Schema.Types.ObjectId,
        ref: "GroupModule",
        required: true    
    }],
    is_agency: { // id của đại lý
        type:  mongoose.Schema.Types.ObjectId,
        ref: "Agency",
        default: null
    },
    is_online: {
        type: Boolean,
        default: false
    },
    is_changed: {
        type: Boolean,
        default: false
    },
    is_updated: {
        type: Boolean,
        default: false
    },
	"log_login" : [{
        type: String,
        default: null
    }],
	"log_logout" :  [{
        type: String,
        default: null
    }],
    day_create_date: {
        type: Date,
        required: true
    },
    day_update_date: {
        type: Date,
    },
    day_create_string: {
        type: String,
        required: true
    },
    day_update_string : {
        type: String,
        default: null
    },
    user_create: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        required: true
    },
    user_update: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        default: null
    },
    temp: {
        type:  Object,
        default: null
    },
    date_online: {
        type:  Number,
        default: 0
    }
});

const Profile = module.exports = mongoose.model("Profile", ProfileSchema);

module.exports.insertProfile = (newProfile, callback) => {
    newProfile.save(callback);
}

module.exports.getAll = (skipItem, limitItem, callback) => {
    let dataProcess = processSkipLimit(skipItem, limitItem);
    let limit = dataProcess.limit;
    let skip = dataProcess.skip;
    let query = { "is_agency" : null }
    Profile.find(query)
        .populate({"path": "group_user", "select": "group_user_name"})
        .populate({"path": "group_parent", "select": "group_user_name"})
        .populate({"path": "permissions", "select": "permission_name"})
        .populate({"path":"group_modules", "select": "group_name"})
        .populate({"path": "user_create", "select": "full_name"})
        .populate({"path": "user_update", "select": "full_name"})
        .skip(skip).limit(limit).exec(callback);
}

module.exports.sumQuery = (dataSearch, callback) => {
    let query = {"user_name" : new RegExp(dataSearch, 'i')};
    Profile.find(query).count(callback);
}

module.exports.sum = (callback) => {
    Profile.count(callback);
}

module.exports.deleteById = (profileId, callback) => {
    Profile.remove({"_id": mongoose.Types.ObjectId(profileId)}, callback);
};
module.exports.updateByeId = (profileId, dataUpdate, callback) => {
    let query = {"_id": mongoose.Types.ObjectId(profileId)};
    Profile.update(query, {$set: dataUpdate}, { multi:true}).exec(callback);
};


module.exports.takeProfileByUsername = (username, callback) => {
    let query =  { $and:[{ "is_agency" : null  }, { "user_name": username }]};
    Profile.findOne(query).populate({
        path: "group_modules",
        populate: { path: "modules",        
            populate: { path: "permissions", select: "permission_name_slug"}    
        }
    }).exec(callback);
}

module.exports.takeModuleByProfileId = (profileId, callback) => {
    let query = {"_id": mongoose.Types.ObjectId(profileId)};
    Profile.findOne(query, callback);
};

module.exports.checkUserName = (username, callback) => {
    let query = { "user_name": username }
    Profile.findOne(query).exec(callback);
}

module.exports.takeDataSearchSkipLimit = (skipItem, limitItem, dataSearch, callback) => {
    let dataProcess = processSkipLimit(skipItem, limitItem);
    let limit = dataProcess.limit;
    let skip = dataProcess.skip;
    let query = {"user_name" : new RegExp(dataSearch, 'i')};
    Profile.find(query)
        .populate({"path": "group_user", "select": "group_user_name"})
        .populate({"path": "group_parent", "select": "group_user_name"})
        .populate({"path": "permissions", "select": "permission_name"})
        .populate({"path":"group_modules", "select": "group_name"})
        .populate({"path": "user_create", "select": "full_name"})
        .populate({"path": "user_update", "select": "full_name"})
        .skip(skip).limit(limit).exec(callback);
}

module.exports.takeProfileByFullName = (fullName, callback) => {
    let query = { "full_name": fullName };
    Profile.findOne(query).exec(callback);
}

module.exports.takeProfileByUserName = (userName, callback) => {
    let query = { "user_name": userName };
    Profile.findOne(query).exec(callback);
}

module.exports.takeProfileByIdAndUserName = (id, userName, callback) =>{
    const query = { $and:[ { "_id":{$ne: mongoose.Types.ObjectId(id)} }, { "user_name": userName } ] };
    Profile.findOne(query).exec(callback);
};

module.exports.takeProfileById = (id, callback) =>{
    const query = { "_id":mongoose.Types.ObjectId(id) };
    Profile.findOne(query).exec(callback);
};

//Agency
module.exports.takeAgencyProfile = (skipItem, limitItem, callback) => {
    let dataProcess = processSkipLimit(skipItem, limitItem);
    let limit = dataProcess.limit;
    let skip = dataProcess.skip;
    let query = { "is_agency" : { $ne : null } };
    Profile.find(query)
        .populate({"path": "group_user", "select": "group_user_name"})
        .populate({"path": "group_parent", "select": "group_user_name"})
        .populate({"path": "permissions", "select": "permission_name"})
        .populate({"path":"group_modules", "select": "group_name"})
        .populate({"path": "user_create", "select": "full_name"})
        .populate({"path": "user_update", "select": "full_name"})
        .populate({"path": "is_agency"})
        .skip(skip).limit(limit).exec(callback);
}

module.exports.sumAgency = (callback) => {
    let query = { "is_agency" : { $ne : null } };
    Profile.find(query).count(callback);
}

module.exports.takeDataSearchAgencyProfile = (skipItem, limitItem, dataSearch, callback) => {
    let dataProcess = processSkipLimit(skipItem, limitItem);
    let limit = dataProcess.limit;
    let skip = dataProcess.skip;
    let query = {$and:[{ "is_agency" : { $ne : null } }, {"user_name" : new RegExp(dataSearch, 'i')}]};
    Profile.find(query)
    .populate({"path": "group_user", "select": "group_user_name"})
    .populate({"path": "group_parent", "select": "group_user_name"})
    .populate({"path": "permissions", "select": "permission_name"})
    .populate({"path": "group_modules", "select": "group_name"})
    .populate({"path": "user_create", "select": "full_name"})
    .populate({"path": "user_update", "select": "full_name"})
    .populate({"path": "is_agency"})
    .skip(skip).limit(limit).exec(callback);
}

module.exports.sumDataSearchAgency = (dataSearch, callback) => {
    let query = {$and:[{ "is_agency" : { $ne : null } }, {"user_name" : new RegExp(dataSearch, 'i')}]};;
    Profile.find(query).count(callback);
}

module.exports.takeAgencyById = (id, callback) => {
    let query = { $and:[ { "is_agency" : { $ne : null } }, { "_id": mongoose.Types.ObjectId(id)} ]};
    Profile.findOne(query)
    .populate({"path": "group_user", "select": "group_user_name"})
    .populate({"path": "group_parent", "select": "group_user_name"})
    .populate({"path": "permissions", "select": "permission_name"})
    .populate({"path": "group_modules", "select": "group_name"})
    .populate({"path": "user_create", "select": "full_name"})
    .populate({"path": "user_update", "select": "full_name"})
    .populate({"path": "is_agency"}).exec(callback);
}

module.exports.takeAgencyByIdAndUserName = (id, userName, callback) => {
    let query = { $and:[{ "is_agency" : { $ne : null } }, { "_id":{$ne: mongoose.Types.ObjectId(id)} }, { "user_name": userName }] };
    Profile.findOne(query)
    .populate({"path": "is_agency"}).exec(callback);
}

module.exports.takeAllAgency = (callback) => {
    let query = {$and:[{ "is_agency" : { $ne : null } }]};
    Profile.find(query)
        .populate({"path": "group_user", "select": "group_user_name"})
        .populate({"path": "group_parent", "select": "group_user_name"})
        .populate({"path": "permissions", "select": "permission_name"})
        .populate({"path":"group_modules", "select": "group_name"})
        .populate({"path": "user_create", "select": "full_name"})
        .populate({"path": "user_update", "select": "full_name"})
        .populate({"path": "is_agency"})
        .exec(callback);
}

module.exports.takeProfileChange = (profileId, changeStatus, callback) => {
    let query = {"_id": mongoose.Types.ObjectId(profileId), "is_changed": changeStatus};
    Profile.findOne(query).exec(callback);
};

module.exports.takeProfileByIdWithData = (profileId, callback) => {
    let query =  { $and:[{ "is_agency" : null  }, { "_id": profileId }]};
    Profile.findOne(query).populate({
        path: "group_modules",
        populate: { path: "modules",        
            populate: { path: "permissions", select: "permission_name_slug"}    
        }
    }).exec(callback);
}