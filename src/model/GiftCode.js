const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const processSkipLimit = require("./../utilities/processSkipLimit");

const GiftCodeSchema = mongoose.Schema({
    code:{ //mã code
        type: String,
        required: true
    },
    pin_code: { //mã hiển thị
        type: String,
        required: true
    },
    "agency_package": {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'AgencyPackage',
        required: true
    },
    "agency_package_name": { // tên gói và đại lý
        type: String,
        required: true
    },
    "package_name": { // tên gói
        type: String,
        required: true
    },
    "price_original": { 
        type: Number,
        required: true
    },
    "price_after_cycle": { 
        type: Number,
        required: true
    },
    "serial": { //mã đại lý
        type: String,
        required: true
    },
    "plan_id": { 
        type: Number,
        required: true
    },
    "created_date": {
        type: String,
        required: true
    },
    "expired_date":{
        type: String,
        required: true
    },
    "primitive_data": { // data nhận về từ payment
        type: Object,
        required: true
    },
    "agency": { //đại lý mua gói
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        required: true
    },
    "agency_child": { //đại lý con
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        default: null
    },
    "use_form": { // hình thức mua đầu tiên
        type: String,
        default: null
    },
    "use_form_2": { // hình thức sử dụng của đại lý con
        type: String,
        default: null
    },
    "active_status": { // code đã được kích hoạt
        type: Boolean,
        default: false
    },
    "time_create_epoch":{
        type: Number,
        required: true
    },
    //
    "day_create_date": {
        type: Date,
        required: true
    },
    "day_create_string": {
        type: String,
        required: true
    },
    "day_create_epoch": {
        type: Number,
        required: true
    },
    "user_create": {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        required: true
    },
    "temp": {
        type:  Object,
        default: null
    }
});

const GiftCode = module.exports = mongoose.model("GiftCode", GiftCodeSchema);

module.exports.getAgencyPackage = (agencyPackageId, callback) => {
    let query = { "agency_package": mongoose.Types.ObjectId(agencyPackageId) };
    GiftCode.findOne(query).count(callback);
}

module.exports.getAll = (skipItem, limitItem, callback) => {
    let dataProcess = processSkipLimit(skipItem, limitItem);
    let limit = dataProcess.limit;
    let skip = dataProcess.skip;
    GiftCode.find()
        .populate({ "path" : "user_create", "select": "full_name" })
        .populate({ "path" : "user_update", "select": "full_name" })
        .sort({"day_create_date": -1})
        .skip(skip).limit(limit).exec(callback);
}

module.exports.sum = (callback) => {
    GiftCode.find().count(callback);
}

module.exports.takeDataSearchSkipLimit = (skipItem, limitItem, dataSearch, callback) => {
    let dataProcess = processSkipLimit(skipItem, limitItem);
    let limit = dataProcess.limit;
    let skip = dataProcess.skip;
    let query = {"pin_code" : new RegExp(dataSearch, 'i')};
    GiftCode.find(query)
        .populate({ "path" : "user_create", "select": "full_name" })
        .populate({ "path" : "user_update", "select": "full_name" })
        .sort({"day_create_date": -1})
        .skip(skip).limit(limit).exec(callback);
}

module.exports.sumQuery = (dataSearch, callback) => {
    let query = {"pin_code" : new RegExp(dataSearch, 'i')}
    GiftCode.find(query).count(callback);
}

module.exports.updateById = (id, dataUpdate, callback) => {
    let query = {"code": id};
    GiftCode.update(query, {$set: dataUpdate}, { multi:true}, callback);
};