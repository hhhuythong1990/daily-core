const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const processSkipLimit = require("./../utilities/processSkipLimit");

const PackageSchema = mongoose.Schema({
    package_name: {
        type: String,
        unique: true,
        required: true
    },
    plan_id: {
        type: Number,
        required: true
    },
    package_price: {
        type: Number,
        required: true
    },
    package_description: {
        type: String,
        required: true
    },
    package_status: {
        type: Boolean,
        required: true
    },
    day_create_date: {
        type: Date,
        required: true
    },
    day_update_date: {
        type: Date,
        default: null
    },
    day_create_string: {
        type: String,
        required: true
    },
    day_update_string: {
        type: String,
        default: null
    },
    user_create: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        required: true
    },
    user_update: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        default: null
    },
    temp: {
        type:  Object,
        default: null
    }
});

const Package = module.exports = mongoose.model("Package", PackageSchema);

module.exports.insertPackage = (newPackage, callback) => {
    newPackage.save(callback);
}

module.exports.takeAllPackage = (callback) => {
    Package.find(callback);
}

module.exports.getByDataSearchSkipLimit = (skipItem, limitItem, dataSearch, callback) => {
    let dataProcess = processSkipLimit(skipItem, limitItem);
    let limit = dataProcess.limit;
    let skip = dataProcess.skip;
    let query = {"package_name" : new RegExp(dataSearch, 'i')};
    Package.find(query)
        .populate({ "path" : "user_create", "select": "full_name" })
        .populate({ "path" : "user_update", "select": "full_name" })
        .skip(skip).limit(limit).exec(callback);
}

module.exports.getBySkipLimit = (skipItem, limitItem, callback) => {
    let dataProcess = processSkipLimit(skipItem, limitItem);
    let limit = dataProcess.limit;
    let skip = dataProcess.skip;
    Package.find()
        .populate({ "path" : "user_create", "select": "full_name" })
        .populate({ "path" : "user_update", "select": "full_name" })
        .skip(skip).limit(limit).exec(callback);
}

module.exports.sum = (callback) => {
    Package.count(callback);
}

module.exports.sumQuery = (dataSearch, callback) => {
    let query = {"package_name" : new RegExp(dataSearch, 'i')};
    Package.find(query).count(callback);
}

module.exports.deleteById = (Id, callback) => {
    Package.remove({"_id": mongoose.Types.ObjectId(Id)}, callback);
};

module.exports.updateById = (id, dataUpdate, callback) => {
    let query = { "_id": mongoose.Types.ObjectId(id)};
    Package.update(query, {$set: dataUpdate}, { multi:true }, callback);
};

module.exports.getAllPackage = (callback) => {
    Package.find().exec(callback);
}

module.exports.takePackageByPackageName = (packageName, callback) => {
    let query = { "package_name": packageName };
    Package.findOne(query).exec(callback);
}

module.exports.takePackageByPlanId = (planId, callback) => {
    let query = { "plan_id": planId };
    Package.findOne(query).exec(callback);
}

module.exports.takePackageById = (packageId, callback) => {
    let query = { "_id": mongoose.Types.ObjectId(packageId)};
    Package.findOne(query).exec(callback);
}

module.exports.checkPackageByPlanId = (packageId, planId, callback) =>{
    const query ={ $and:[{"_id":{$ne: mongoose.Types.ObjectId(packageId)}},{"plan_id": planId}] };
    Package.findOne(query).exec(callback);
};

module.exports.checkPackageByPackageName = (packageId, packageName, callback) =>{
    const query ={ $and:[{"_id":{$ne: mongoose.Types.ObjectId(packageId)}},{"package_name": packageName}] };
    Package.findOne(query).exec(callback);
};

module.exports.checkPackageForUpdate = (packageId, packageName, planId, callback) =>{
    const query = {$or : [{ $and:[{"_id":{$ne: mongoose.Types.ObjectId(packageId)}},{"package_name": packageName}] } ,
    { $and:[{"_id":{$ne: mongoose.Types.ObjectId(packageId)}},{"plan_id": planId}] }] };
    Package.findOne(query).exec(callback);
};

module.exports.checkPackageForCreate = (packageName, planId, callback) =>{
    const query = {$or : [{"package_name": packageName},{"plan_id": planId} ] };
    Package.findOne(query).exec(callback);
};

module.exports.takeAllPackageActive = (callback) => {
    const query = {"package_status": true};
    Package.find(query)
        .populate({ "path" : "user_create", "select": "full_name" })
        .populate({ "path" : "user_update", "select": "full_name" })
        .exec(callback);
}