const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const processSkipLimit = require("./../utilities/processSkipLimit");

const AgencyPackageSchema = mongoose.Schema({
    agency_package_name:{
        type: String,
        required:true
    },
    agency: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        required: true
    },
    package: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Package',
        required: true
    },
    cycle: { // chiếc khấu
        type: Number,
        required:true
    },
    price_after_cycle:{
        type: Number,
        required:true
    },
    agency_package_status: {
        type: Boolean,
        required: true
    },
    day_create_date: {
        type: Date,
        required: true
    },
    day_update_date: {
        type: Date,
        default: null
    },
    day_create_string: {
        type: String,
        required: true
    },
    day_update_string: {
        type: String,
        default: null
    },
    user_create: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        required: true
    },
    user_update: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        default: null
    },
    temp: {
        type:  Object,
        default: null
    }
});

const AgencyPackage = module.exports = mongoose.model("AgencyPackage", AgencyPackageSchema);

module.exports.insertAgencyPackage = (newAgencyPackage, callback) => {
    newAgencyPackage.save(callback);
}

module.exports.takeAgencyPackageByAgencyAndPackage = (agencyId, packageId, callback) => {
    let  query = { $and : [{ "agency": mongoose.Types.ObjectId(agencyId)} , { "package":mongoose.Types.ObjectId(packageId)} ] }
    AgencyPackage.findOne(query).exec(callback);
}

module.exports.getAll = (skipItem, limitItem, callback) => {
    let dataProcess = processSkipLimit(skipItem, limitItem);
    let limit = dataProcess.limit;
    let skip = dataProcess.skip;
    AgencyPackage.find()
        .populate({
            path: "agency",
            select: "full_name",
            populate: { path: "is_agency"}
        })
        .populate("package")
        .populate({ "path" : "user_create", "select": "full_name" })
        .populate({ "path" : "user_update", "select": "full_name" })
        .skip(skip).limit(limit)
        .sort({"day_create_date": 1}).exec(callback);
}

module.exports.sum = (callback) => {
    AgencyPackage.count(callback);
}

module.exports.takeDataSearchSkipLimit = (skipItem, limitItem, dataSearch, callback) => {
    let dataProcess = processSkipLimit(skipItem, limitItem);
    let limit = dataProcess.limit;
    let skip = dataProcess.skip;
    let query = {"agency_package_name" : new RegExp(dataSearch, 'i')};
    AgencyPackage.find(query)
        .populate({
            path: "agency", 
            select: "full_name",
            populate: { path: "is_agency"}
        })
        .populate("package")
        .populate({ "path" : "user_create", "select": "full_name" })
        .populate({ "path" : "user_update", "select": "full_name" })
        .skip(skip).limit(limit).exec(callback);
};

module.exports.sumQuery = (dataSearch, callback) => {
    let query = {"agency_package_name" : new RegExp(dataSearch, 'i')};
    AgencyPackage.find(query).count(callback);
}

module.exports.takeAgencyPackageById = (id, callback) => {
    let query = {"_id": mongoose.Types.ObjectId(id)};
    AgencyPackage.findOne(query, callback);
};

module.exports.takeAgencyPackageByIdAgencyAndPackage = (id, agencyId, packageId, callback) => {
    let  query = { $and : [{"_id":{$ne: mongoose.Types.ObjectId(id)}}, { "agency": mongoose.Types.ObjectId(agencyId)}, { "package":mongoose.Types.ObjectId(packageId)} ] }
    AgencyPackage.findOne(query).exec(callback);
}

module.exports.updateAgencyPackageById = (id, dataUpdate, callback) => {
    let query = {"_id": mongoose.Types.ObjectId(id)};
    AgencyPackage.update(query, {$set: dataUpdate}, { multi:true}, callback);
};

module.exports.deleteId = (agencyPackageId, callback) => {
    AgencyPackage.remove({"_id": mongoose.Types.ObjectId(agencyPackageId)}, callback);
}

module.exports.takeAgencyPackageByAgency = (agencyId, callback) => {
    let query = {"agency": mongoose.Types.ObjectId(agencyId)};
    AgencyPackage.findOne(query).exec(callback);
}

module.exports.takeAgencyPackageByPackage = (packageId, callback) => {
    let query = {"package": mongoose.Types.ObjectId(packageId)};
    AgencyPackage.findOne(query).exec(callback);
}