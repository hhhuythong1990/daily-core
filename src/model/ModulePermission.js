const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const processSkipLimit = require("./../utilities/processSkipLimit");
const constants = require("./../utilities/Constants");

const ModulePermissionSchema = mongoose.Schema({
    module_permission_name: {
        type: String,
        required: true,
        unique: true
    },
    module: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Module",
        required:true
    },
    permissions: [{
        type:  mongoose.Schema.Types.ObjectId,
        ref: "Permission",
        required:true
    }],
    is_publish: {
        type: Boolean,
        required: true
    },
    day_create_date: {
        type: Date,
        required: true
    },
    day_create_string: {
        type: String,
        required: true
    },
    day_update_date: {
        type: Date,
        default: null
    },
    day_update_string : {
        type: String,
        default: null
    },
    user_create: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        required: true
    },
    user_update: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        default: null
    },
    temp: {
        type:  Object,
        default: null
    }
});

const ModulePermission = module.exports = mongoose.model("ModulePermission", ModulePermissionSchema);

module.exports.insertModulePermission = (newModulePermission, callback) => {
    newModulePermission.save(callback);
};

module.exports.takeModulePermissionById = (modulePermissionId, callback) => {
    let query = {"module": mongoose.Types.ObjectId(modulePermissionId)};
    ModulePermission.findOne(query, callback);
};

module.exports.takeModulePermissionByName = (modulePermissionName, callback) => {
    let query = {"module_permission_name": modulePermissionName};
    ModulePermission.findOne(query, callback);
};

module.exports.takeModulePermissionBySkipLimit = (skipItem, limitItem, callback) => {
    let dataProcess = processSkipLimit(skipItem, limitItem);    
    let limit = dataProcess.limit;
    let skip = dataProcess.skip;
    ModulePermission.find()
        .populate("module", "module_name")
        .populate("permissions", "permission_name")
        .populate({ "path" : "user_create", "select": "full_name" })
        .populate({ "path" : "user_update", "select": "full_name" })
        .skip(skip).limit(limit).exec(callback);
};

module.exports.sumModulePermission = (callback) => {
    ModulePermission.count(callback);
};

module.exports.deleteById = (id, callback) => {
    ModulePermission.remove({"_id": mongoose.Types.ObjectId(id)}, callback);
};

module.exports.updateByeId = (id, dataUpdate, callback) => {
    let query = {"_id": mongoose.Types.ObjectId(id)};
    ModulePermission.update(query, {$set: dataUpdate}, { multi:true}, callback);
};

module.exports.takeModulePermissionById = (modulePermissionId, callback) => {
    let query = { "_id": modulePermissionId };
    ModulePermission.findOne(query).populate("module", "module_name")
    .populate("permissions", "permission_name").exec(callback);
}

module.exports.takeModulePermissionShow = (callback) => {
    let query = {"is_publish" : constants.IS_TRUE}
    ModulePermission.find(query)
    .populate("module", "module_name")
    .populate("permissions", "permission_name").exec(callback);
}

module.exports.takeModulePermissionAll = (callback) => {
    let query = {"is_publish" : constants.IS_FALSE}
    ModulePermission.find(query)
    .populate("module", "module_name")
    .populate("permissions", "permission_name").exec(callback);
}

module.exports.takeModulePermissionByIdAndName = (modulePermissionId, modulePermissionName, callback) => {
    const query ={$and:[{"_id":{$ne: mongoose.Types.ObjectId(modulePermissionId)}}, {"module_permission_name": modulePermissionName}]};
    ModulePermission.findOne(query, callback);
};