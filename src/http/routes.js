const middleware = require("./../utilities/Authentication");
// c2
module.exports =  function(app){
    routerConfig = {
        /*Profile */
        '/api/profile/create': ['post','../controller/ProfileController','create'],
        '/api/profile/takeAll': ['get','../controller/ProfileController','getAll'],
        '/api/profile/takeProfile': ['get','../controller/ProfileController','getProfileByProfileId'],
        '/api/profile/remove': ['post','../controller/ProfileController','remove'],
        '/api/profile/update': ['post','../controller/ProfileController','update'],
        '/api/profile/authentication': ['post','../controller/ProfileController','authenticationUserNamePassword'],
        '/api/profile/dataRelation': ['get','../controller/ProfileController.js','getDataRelationProfile'],
        '/api/profile/takeProfileByFullName': ['post','../controller/ProfileController','getProfileByFullName'],
        '/api/profile/takeProfileByUserName': ['post','../controller/ProfileController','getProfileByUserName'],
        '/api/profile/takeProfileByIdAndUserName': ['post','../controller/ProfileController','getProfileByIdAndUserName'],
        '/api/profile/takeByDataSearchSkipLimit': ['get','../controller/ProfileController','getByDataSearchSkipLimit'],
        '/api/profile/changePassword': ['post','../controller/ProfileController','changePassword'],
        '/api/profile/changeOnline': ['post','../controller/ProfileController','changeOnline'],
        '/api/profile/changeBlock': ['post','../controller/ProfileController','changeBlock'],
        '/api/profile/logout': ['post','../controller/ProfileController','logout'],
        '/api/profile/takeProfileChange': ['post','../controller/ProfileController','takeProfileChange'],

        /* Group Module */
        '/api/groupModule/create': ['post','../controller/GroupModuleController','create'],
        '/api/groupModule/takeBySkipLimit': ['get','../controller/GroupModuleController','getBySkipLimit'],
        '/api/groupModule/takeGroupModule': ['get','../controller/GroupModuleController','getGroupModuleByGroupModuleId'],
        '/api/groupModule/takeByDataSearchSkipLimit': ['get','../controller/GroupModuleController','getByDataSearchSkipLimit'],
        '/api/groupModule/takeAll': ['get','../controller/GroupModuleController','getAll'],
        '/api/groupModule/remove': ['post','../controller/GroupModuleController','remove'],
        '/api/groupModule/update': ['post','../controller/GroupModuleController','update'],
        '/api/groupModule/takeGroupModuleByIdAndGroupModuleName': ['post','../controller/GroupModuleController','getGroupModuleByIdAndGroupModuleNameDescription'],
        '/api/groupModule/takeGroupModuleByGroupModuleName': ['post','../controller/GroupModuleController','getGroupModuleDescription'],

        /* Module */
        '/api/module/create': ['post','../controller/ModuleController','create'],
        '/api/module/takeBySkipLimit': ['get','../controller/ModuleController','getBySkipLimit'],
        '/api/module/takeByDataSearchSkipLimit': ['get','../controller/ModuleController','getByDataSearchSkipLimit'],
        '/api/module/takeAll': ['get','../controller/ModuleController','getAll'],
        '/api/module/takeModule': ['get','../controller/ModuleController','getModuleByModuleId'],
        '/api/module/remove': ['post','../controller/ModuleController','remove'],
        '/api/module/update': ['post','../controller/ModuleController','update'],
        '/api/module/takeModuleByModuleName': ['post','../controller/ModuleController','getModuleByModuleName'],
        '/api/module/takeModuleByIdAndModuleName': ['post','../controller/ModuleController','getModuleByIdAndModuleName'],

        /* Group User */
        '/api/groupUser/create': ['post','../controller/GroupUserController.js','create'],
        '/api/groupUser/takeBySkipLimit': ['get','../controller/GroupUserController.js','getBySkipLimit'],
        '/api/groupUser/remove': ['post','../controller/GroupUserController.js','remove'],
        '/api/groupUser/update': ['post','../controller/GroupUserController.js','update'],
        '/api/groupUser/takeGroupAdmin': ['post','../controller/GroupUserController.js','getGroupAdmin'],

        /*Permission*/
        '/api/permission/create': ['post','../controller/PermissionController.js','create'],
        '/api/permission/takeBySkipLimit': ['get','../controller/PermissionController.js','getBySkipLimit'],
        '/api/permission/takeByDataSearchSkipLimit': ['get','../controller/PermissionController.js','getByDataSearchSkipLimit'],
        '/api/permission/remove': ['post','../controller/PermissionController.js','remove'],
        '/api/permission/update': ['post','../controller/PermissionController.js','update'],
        // '/api/permission/takeAllPermission': ['get','../controller/PermissionController.js','getAllPermission'],
        '/api/permission/takePermissionById': ['get','../controller/PermissionController.js','getPermissionById'],
        '/api/permission/takePermissionByIdAndPermissionName': ['post','../controller/PermissionController.js','getPermissionByIdAndPermissionName'],
        '/api/permission/takePermissionByPermissionName': ['post','../controller/PermissionController.js','getPermissionByPermissionName'],

        /*Role*/
        '/api/role/create': ['post','../controller/RoleController.js','create'],
        '/api/role/getAll': ['get','../controller/RoleController.js','getAll'],
        '/api/role/remove': ['post','../controller/RoleController.js','remove'],
        '/api/role/update': ['post','../controller/RoleController.js','update'],

        /*Agency*/
        '/api/agency/create': ['post','../controller/AgencyController.js','create'],
        '/api/agency/update': ['post','../controller/AgencyController.js','update'],
        '/api/agency/takeAgencyByAgencyCode': ['post','../controller/AgencyController.js','getAgencyByAgencyCode'],
        '/api/agency/takeAgencyBySkipLimit': ['get','../controller/AgencyController.js','getAllAgency'],
        '/api/agency/takeDataSearchAgency': ['get','../controller/AgencyController.js','getDataSearchAgency'],
        '/api/agency/takeAgencyById': ['get','../controller/AgencyController.js','getAgencyByAgencyId'],
        '/api/agency/takeAgencyByIdAndUsername': ['post','../controller/AgencyController.js','getAgencyByIdAndUsername'],
        '/api/agency/dataRelation': ['get','../controller/AgencyController.js','getDataRelationAgency'],

        /*Package*/
        '/api/package/create': ['post','../controller/PackageController.js','create'],
        '/api/package/update': ['post','../controller/PackageController.js','update'],
        '/api/package/takePackageByPackageName': ['post','../controller/PackageController.js','getPackageByPackageName'],
        '/api/package/takePackageByPlanId': ['post','../controller/PackageController.js','getPackageByPlanId'],
        '/api/package/takeBySkipLimit': ['get','../controller/PackageController.js','getBySkipLimit'],
        '/api/package/takeByDataSearchSkipLimit': ['get','../controller/PackageController.js','getByDataSearchSkipLimit'],
        '/api/package/getPackageById': ['get','../controller/PackageController.js','getPackageById'],
        '/api/package/takePackageByIdAndPlanId': ['post','../controller/PackageController.js','getPackageByIdAndPlanId'],
        '/api/package/takePackageByIdAndPackageName': ['post','../controller/PackageController.js','getPackageByIdAndPackageName'],
        '/api/package/remove': ['post','../controller/PackageController.js','remove'],

        /*Agency and Package */
        '/api/agencyPackage/dataRelation': ['get','../controller/AgencyPackageController.js','getDataRelation'],
        '/api/agencyPackage/create': ['post','../controller/AgencyPackageController.js','create'],
        '/api/agencyPackage/takeBySkipLimit': ['get','../controller/AgencyPackageController.js','getBySkipLimit'],
        '/api/agencyPackage/takeByDataSearchSkipLimit': ['get','../controller/AgencyPackageController.js','getByDataSearchSkipLimit'],
        '/api/agencyPackage/takeAgencyPackageById': ['get','../controller/AgencyPackageController.js','getAgencyPackageById'],
        '/api/agencyPackage/update': ['post','../controller/AgencyPackageController.js','update'],
        '/api/agencyPackage/remove': ['get','../controller/AgencyPackageController.js','remove'],

        /*Code */
        '/api/code/takeBySkipLimit': ['get','../controller/GiftCodeController.js','getBySkipLimit'],
        '/api/code/takeByDataSearchSkipLimit': ['get','../controller/GiftCodeController.js','getByDataSearchSkipLimit'],
        '/api/code/activeCode': ['post','../controller/GiftCodeController.js','updateActiveStatus'],
        
        /*Module Permission */
        '/api/modulePermission/dataRelation': ['get','../controller/ModulePermissionController.js','getDataModulePermission'],
        '/api/modulePermission/takeBySkipLimit': ['get','../controller/ModulePermissionController.js','getBySkipLimit'],
        '/api/modulePermission/create': ['post','../controller/ModulePermissionController.js','create'],
        '/api/modulePermission/getModulePermissionById': ['get','../controller/ModulePermissionController.js','getModulePermissionById'],
        '/api/modulePermission/update': ['post','../controller/ModulePermissionController.js','update'],
        '/api/modulePermission/remove': ['post','../controller/ModulePermissionController.js','remove'],

        /*Request Gift Code */
        '/api/requestGiftCode/getData': ['post','../controller/RequestGiftCodeController.js','getAllRequestCode'],
        '/api/requestGiftCode/export': ['post','../controller/RequestGiftCodeController.js','exportRequestCode'],
        
    }
    require('./router')(app, routerConfig);
}

//c1:
// module.exports = function(app){
//     app.use("/api", require("../controller/ProfileController"));
//     app.use("/api", require("../controller/ModuleController"));
//     app.use("/api", require("../controller/GroupModuleController"));
// }
