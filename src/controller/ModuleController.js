const moment = require("moment");
const async = require("async");

const messageValidate = require("./../utilities/MessageValidate");
const messageResponse = require("./../utilities/MessageResponse");
const constants =  require("./../utilities/Constants");

const Module = require("./../model/Module");
exports.create = (req, res) => {
    req.check("module_name", messageValidate("module_name")).notEmpty();
    req.check("module_name_slug", messageValidate("module_name_slug")).notEmpty();
    req.check("route_path", messageValidate("route_path")).notEmpty();
    req.check("publish", messageValidate("publish")).notEmpty();
    req.check("user_create", messageValidate("user_create")).notEmpty();
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.json(messageResponse("", result.array()).MISSING_FIELD);
        } else {
            let moduleName = req.body.module_name;
            Module.takeModuleByName(moduleName, (err, existModule) => {
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(existModule){
                        res.json(messageResponse("Mô-dun", moduleName).EXIST_OBJECT);
                    }else{
                        let now = new Date();
                        let newModule = new Module ({
                            "module_name": moduleName,
                            "module_name_slug": req.body.module_name_slug,
                            "route_path": req.body.route_path,
                            "is_publish": req.body.publish,
                            "user_create": req.body.user_create,
                            "day_create_date": now,
                            "day_create_string": moment(now).format(constants.FORMAT_FULL_DATE),
                        });
                        Module.insertModule(newModule, (err, module) => {
                            if(err){
                                res.json(messageResponse(err).QUERY_ERROR);
                            }else{                 
                                res.json(messageResponse("mô-dun", null).ADD_SUCCESSFULLY);                        
                            }
                        });
                    }
                }
            });
        }
    }).catch(function(rea){
        res.json(rea);
    });
}
exports.getBySkipLimit = (req,res) => {
    req.check("skip", messageValidate("skip")).notEmpty();
    req.check("limit", messageValidate("limit")).notEmpty();
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.json(messageResponse("", result.array()).MISSING_FIELD);
        } else {
            Module.sumModules((err, totalModules) => {
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(totalModules.length == 0){
                        res.json(messageResponse(null, null).NOT_FOUND);
                    }else{
                        Module.takeModuleBySkipLimit(req.query.skip, req.query.limit, (err, listModule) => {
                            if(err){
                                res.json(messageResponse(err).QUERY_ERROR);
                            }else{
                                res.json(messageResponse(null, listModule, totalModules).FOUND_DATA);
                            }
                        });
                    }
                }
            });
        }
    }).catch(function(rea){
        console.log(rea);
    });
}

exports.getByDataSearchSkipLimit = (req, res) => {
	req.check("limit", messageValidate("limit")).notEmpty();
    req.check("skip", messageValidate("skip")).notEmpty();
    req.check("moduleName", messageValidate("moduleName")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let dataSearch = decodeURI(req.query.moduleName);
            Module.takeDataSearchSkipLimit(req.query.skip, req.query.limit, dataSearch, (err, dataList)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{                    
                    Module.sumQuery(dataSearch, (err, data)=>{
                        if(err){
                            res.json(messageResponse(err).QUERY_ERROR);
                        }else{
                            res.json(messageResponse(null, dataList, data).FOUND_DATA);
                        }
                    });
                }
            });
    	}
    });
}

exports.getAll = (req, res) => {
    Module.takeAllModule((err, modules) => {
        if(err){
            res.json(messageResponse(err).QUERY_ERROR);
        }else{
            if(modules.length == 0){
                res.json(messageResponse(null, null).NOT_FOUND);
            }else {
                res.json(messageResponse(null, modules, null).FOUND_DATA);
            }
        }
    });
}


exports.getModuleByModuleId = (req, res) => {
    req.check("module_id", messageValidate("module_id")).notEmpty();
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.json(messageResponse("", result.array()).MISSING_FIELD);
        } else {
            Module.takeModuleByModuleId(req.query.module_id, (err, module) => {
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(!module){
                        return res.json(messageResponse(null, null).NOT_FOUND);
                    }else{
                        res.json(messageResponse(null, module, null).FOUND_DATA);                       
                    }
                }
            });
        }
    }).catch(function(rea){
        console.log(rea);
    });
}

exports.remove = (req,res) => {
    req.check("module_id", messageValidate("module_id")).notEmpty();
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
            res.json(messageResponse("", result.array()).MISSING_FIELD);
        } else {
            Module.deleteModuleByModuleId(req.body.module_id,(err)=>{
                if(err){
                    res.json(messageResponse(null,null).DELETE_FAIL);
                }else {                
                    res.json(messageResponse(null, null).DELETE_SUCCESSFULLY);
                }
            });
        }
    }).catch(function(rea){
        res.json(rea);
    });   
}

exports.update = (req, res) => {
    req.check("module_id", messageValidate("module_id")).notEmpty();
    req.check("module_name", messageValidate("module_name")).notEmpty();
    req.check("module_name_slug", messageValidate("module_name_slug")).notEmpty();
    req.check("route_path", messageValidate("route_path")).notEmpty();
    req.check("publish", messageValidate("publish")).notEmpty();
    req.check("user_update", messageValidate("user_update")).notEmpty();
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
            res.json(messageResponse("", result.array()).MISSING_FIELD);
        } else {
            let moduleName = req.body.module_name;
            let moduleId = req.body.module_id;
            Module.checkModuleByModuleName(moduleId, moduleName, (err, existModule) => {                
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);        
                }else {
                    if(existModule){
                        res.json(messageResponse("Tên mô-đun", null).EXIST_OBJECT);
                    }else {                        
                        let now = new Date();
                        let dataUpdate = {
                            "module_name" :  moduleName,
                            "module_name_slug": req.body.module_name_slug,
                            "route_path": req.body.route_path,
                            "is_publish": req.body.publish,
                            "day_update_date": now,                        
                            "day_update_string": moment(now).format(constants.FORMAT_FULL_DATE),
                            "user_update": req.body.user_update
                        }  
                        Module.updateModuleByModuleId(moduleId, dataUpdate,(err,raw)=>{ 
                            if(err){
                                res.json(messageResponse(err).QUERY_ERROR);
                            }else{
                                res.json(messageResponse("mô-đun", null).UPDATE_SUCCESSFULLY);
                            }
                        });
                    }
                }
            });
        }
    })   
}
exports.getModuleByIdAndModuleName = (req, res) => {
    req.check("module_id", messageValidate("module_id")).notEmpty();
    req.check("module_name", messageValidate("module_name")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            Module.checkModuleByModuleName(req.body.module_id, req.body.module_name,(err, module)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(module){
                        res.json(messageResponse("", module).FOUND_DATA);
                    }else {
                        res.json(messageResponse("", "").NOT_FOUND)
                    }
                }
            });
    	}
    });
}

exports.getModuleByModuleName = (req, res) => {
    req.check("module_name", messageValidate("module_name")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            Module.takeModuleByModuleName(req.body.module_name,(err, module)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(module){
                        res.json(messageResponse("", module).FOUND_DATA);
                    }else {
                        res.json(messageResponse("", "").NOT_FOUND)
                    }
                }
            });
    	}
    });
}
