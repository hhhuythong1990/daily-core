const express = require("express");
const router = express.Router();
const moment = require("moment");
const async = require("async");
const md5 = require("md5");
const _ = require("lodash");

const messageValidate = require("./../utilities/MessageValidate");
const messageResponse = require("./../utilities/MessageResponse");
const constants =  require("./../utilities/Constants");

const Profile = require("./../model/Profile");
const GroupModule = require("./../model/GroupModule");
const Permission = require("./../model/Permission");
const Agency = require("./../model/Agency");
const ModulePermission = require("./../model/ModulePermission");

exports.create = (req, res) =>{
    req.check("full_name", messageValidate("full_name")).notEmpty();
    req.check("user_name", messageValidate("user_name")).notEmpty();
    req.check("password", messageValidate("password")).notEmpty();
    req.check("permissions", messageValidate("permissions")).notEmpty();
    req.check("group_parent", messageValidate("group_parent")).notEmpty();
    req.check("publish", messageValidate("publish")).notEmpty();
    req.check("user_create", messageValidate("user_create")).notEmpty();

    req.check("payment_limit", messageValidate("payment_limit")).notEmpty();
    req.check("agency_code", messageValidate("agency_code")).notEmpty();
    req.check("full_name_represent", messageValidate("full_name_represent")).notEmpty();
    req.check("id_card", messageValidate("id_card")).notEmpty();
    req.check("phone", messageValidate("phone")).notEmpty();
    req.check("email", messageValidate("email")).notEmpty();
    req.check("address", messageValidate("address")).notEmpty();
    req.check("image_id_card_front", messageValidate("image_id_card_front")).notEmpty();
    req.check("image_id_card_behind", messageValidate("image_id_card_behind")).notEmpty();
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let userName = req.body.user_name;
            let agencyCode = req.body.agency_code;
            Profile.checkUserName(userName, function(err, profileExist){
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else {
                    if(profileExist){
                        res.json(messageResponse("Tên đăng nhập", userName).EXIST_OBJECT);
                    }else {
                        Agency.checkAgencyCode(agencyCode, function(err, agencyExist){
                            if(err){
                                res.json(messageResponse(err).QUERY_ERROR);
                            }else {
                                if(agencyExist){
                                    res.json(messageResponse("Mã đại lý", agencyCode).EXIST_OBJECT);
                                }else {
                                    let now = new Date();
                                    let newAgency = new Agency({
                                        "payment_limit": req.body.payment_limit,
                                        "agency_code": agencyCode,
                                        "full_name_represent": req.body.full_name_represent,
                                        "id_card": req.body.id_card,
                                        "phone": req.body.phone,
                                        "email": req.body.email,
                                        "address": req.body.address,
                                        "image_id_card_front": req.body.image_id_card_front,
                                        "image_id_card_behind": req.body.image_id_card_behind
                                    });
                                    Agency.insertAgencyInfo(newAgency, (err, agency) => {
                                        if(err){
                                            res.json(messageResponse(err).QUERY_ERROR);
                                        } else {
                                            
                                            let newProfile = new Profile({
                                                "full_name":  userName,
                                                "user_name":  req.body.user_name,
                                                "password":  md5(req.body.password + constants.SALT),
                                                "permissions": req.body.permissions,
                                                // "group_user": req.body.group_user,
                                                "is_publish": req.body.publish,
                                                "group_modules": req.body.group_modules,
                                                "group_parent": req.body.group_parent,
                                                "user_create": req.body.user_create,
                                                "day_create_date": now,
                                                "is_agency": agency._id,
                                                "day_create_string": moment(now).format(constants.FORMAT_FULL_DATE),
                                            });
                                            Profile.insertProfile(newProfile, (err, data) => {
                                                if(err){
                                                    res.json(messageResponse(err).QUERY_ERROR);
                                                }else{                            
                                                    res.json(messageResponse("đại lý", data).ADD_SUCCESSFULLY);   
                                                }
                                            });

                                        }
                                    });
                                }
                            }
                        });
                    }
                }
            });
        }
    });
}

exports.getAll = (req, res) => {
	req.check("limit", messageValidate("limit")).notEmpty();
    req.check("skip", messageValidate("skip")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            Profile.takeAgencyProfile(req.body.skip, req.body.limit,(err,datalist)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    Profile.sum((err, data)=>{
                        if(err){
                            res.json(messageResponse(err).QUERY_ERROR);
                        }else{
                            res.json(messageResponse(null, datalist, data).FOUND_DATA);
                        }
                    });
                }
            });
    	}
    });
}

exports.getAgencyByAgencyCode = (req, res) => {
    req.check("agency_code", messageValidate("agency_code")).notEmpty();
    req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            Agency.takeAgencyByAgencyCode(req.body.agency_code, (err, agency)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(agency){
                        res.json(messageResponse("", agency).FOUND_DATA);
                    }else {
                        res.json(messageResponse("", "").NOT_FOUND)
                    }
                    
                }
            });
        }
    });
}

exports.getAllAgency = (req, res) => {
	req.check("limit", messageValidate("limit")).notEmpty();
    req.check("skip", messageValidate("skip")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            Profile.takeAgencyProfile(req.query.skip, req.query.limit,(err,datalist)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    Profile.sumAgency((err,data)=>{
                        if(err){
                            res.json(messageResponse(err).QUERY_ERROR);
                        }else{
                            res.json(messageResponse(null, datalist, data).FOUND_DATA);
                        }
                    });
                }
            });
    	}
    });
}
exports.getDataSearchAgency = (req, res) => {
	req.check("limit", messageValidate("limit")).notEmpty();
    req.check("skip", messageValidate("skip")).notEmpty();
    req.check("agency_name", messageValidate("agency_name")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let dataSearch = decodeURI(req.query.agency_name);
            Profile.takeDataSearchAgencyProfile(req.query.skip, req.query.limit, dataSearch, (err, dataList)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    Profile.sumDataSearchAgency(dataSearch, (err, totalData)=>{
                        if(err){
                            res.json(messageResponse(err).QUERY_ERROR);
                        }else{
                            res.json(messageResponse(null, dataList, totalData).FOUND_DATA);
                        }
                    });
                }
            });
    	}
    });
}

exports.getAgencyByAgencyId = (req, res) => {
    req.check("agency_id", messageValidate("agency_id")).notEmpty();
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.json(messageResponse("", result.array()).MISSING_FIELD);
        } else {
            Profile.takeAgencyById(req.query.agency_id, (err, agency) => {
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(!agency){
                        res.json(messageResponse(null, null).NOT_FOUND);
                    }else{
                        res.json(messageResponse(null, agency, null).FOUND_DATA);                       
                    }
                }
            });
        }
    }).catch(function(rea){
        console.log(rea);
    });
}

exports.getAgencyByIdAndUsername = (req, res) => {
    req.check("agency_id", messageValidate("agency_id")).notEmpty();
    req.check("user_name", messageValidate("user_name")).notEmpty();
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.json(messageResponse("", result.array()).MISSING_FIELD);
        } else {
            Profile.takeAgencyByIdAndUserName(req.body.agency_id, req.body.user_name, (err, agency) => {
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(!agency){
                        res.json(messageResponse(null, null).NOT_FOUND);
                    }else{
                        res.json(messageResponse(null, agency, null).FOUND_DATA);                       
                    }
                }
            });
        }
    }).catch(function(rea){
        console.log(rea);
    });
}

exports.getDataRelationAgency = (req, res) => {
    collectionDataAgency ((err, results) => {
        if(err){
            res.json(messageResponse(err).QUERY_ERROR);
        }else{
            res.json(messageResponse("", results).FOUND_DATA);
        }
    });
}

collectionDataAgency = (callback) => {
    async.parallel({
        groupModules: function (callback) {
            getGroupModule(callback);
        },
        permissions: function (callback) {
            getPermissions(callback);
        },
    }, function (err, results) {
        if(err){
            callback(err, null);
        }else{
            callback(null, results);
        }
        
    });
}

getGroupModule = (callback) => {
    GroupModule.takeGroupModuleShow((err, groupModules) => {
		if(err){
			callback(err, null);
		}else {            
			callback(null, groupModules);
		}
	});
}

getPermissions = (callback) => {
    ModulePermission.takeModulePermissionShow((err, permissions) => {
		if(err){
			callback(err, null);
		}else { 
			callback(null, permissions);
		}
	});
}

exports.update = (req, res) =>{
    req.check("profile_id", messageValidate("profile_id")).notEmpty();
    req.check("full_name", messageValidate("full_name")).notEmpty();
    req.check("user_name", messageValidate("user_name")).notEmpty();
    req.check("permissions", messageValidate("permissions")).notEmpty();
    req.check("group_parent", messageValidate("group_parent")).notEmpty();
    req.check("publish", messageValidate("publish")).notEmpty();
    req.check("user_update", messageValidate("user_update")).notEmpty();

    req.check("agency_id", messageValidate("agency_id")).notEmpty();
    req.check("payment_limit", messageValidate("payment_limit")).notEmpty();
    // req.check("agency_code", messageValidate("agency_code")).notEmpty();
    req.check("full_name_represent", messageValidate("full_name_represent")).notEmpty();
    req.check("id_card", messageValidate("id_card")).notEmpty();
    req.check("phone", messageValidate("phone")).notEmpty();
    req.check("email", messageValidate("email")).notEmpty();
    req.check("address", messageValidate("address")).notEmpty();
    // req.check("image_id_card_front", messageValidate("image_id_card_front")).notEmpty();
    // req.check("image_id_card_behind", messageValidate("image_id_card_behind")).notEmpty();
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let agencyId = req.body.agency_id;
            // let agencyCode = req.body.agency_code;
            let profileId = req.body.profile_id;
            let userName = req.body.user_name;
            let password = req.body.password;
            let now = new Date();
            Profile.takeProfileByIdAndUserName(profileId, userName, (err, profile)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else {
                    if(profile){
                        res.json(messageResponse("Tên đăng nhập", null).EXIST_OBJECT);
                    }else {
                        // Agency.takeAgencyByIdAndAgencyCode(agencyId, agencyCode, (err, agency)=>{
                        //     if(err){
                        //         res.json(messageResponse(err).QUERY_ERROR);
                        //     }else {
                        //         if(agency){
                        //             res.json(messageResponse("Mã đại lý", null).EXIST_OBJECT);
                        //         }else {
                                    let imageCardFront = req.body.image_id_card_front;
                                    let imageCardBehind = req.body.image_id_card_behind;
                                    let updateAgency = {
                                        "payment_limit": req.body.payment_limit,
                                        // "agency_code": agencyCode,
                                        "full_name_represent": req.body.full_name_represent,
                                        "id_card": req.body.id_card,
                                        "phone": req.body.phone,
                                        "email": req.body.email,
                                        "address": req.body.address
                                    };
                                    if (imageCardFront !== "") {
                                        updateAgency["image_id_card_front"] = imageCardFront;
                                    }
                                    if (imageCardBehind !== ""){
                                        updateAgency["image_id_card_behind"] = imageCardBehind;
                                    }
                                    Agency.updateByeId(agencyId, updateAgency, (err)=> {
                                        if(err){
                                            res.json(messageResponse(err).QUERY_ERROR);
                                        }else {
                                            let updateProfile = {
                                                "full_name":  req.body.full_name,
                                                "user_name":  userName,
                                                "permissions": req.body.permissions,
                                                "is_publish": req.body.publish,
                                                "group_modules": req.body.group_modules,
                                                "group_parent": req.body.group_parent,
                                                "user_update": req.body.user_update,
                                                "is_changed": true,
                                                "day_update_date": now,
                                                "is_agency":agencyId,
                                                "day_update_string": moment(now).format(constants.FORMAT_FULL_DATE),
                                            };
                                            if (password !== "") {
                                                updateProfile["password"] = md5(password + constants.SALT);
                                            }
                                            Profile.updateByeId(profileId, updateProfile, (err, result)=>{
                                                if(err){
                                                    res.json(messageResponse(err).QUERY_ERROR);
                                                } else{
                                                    res.json(messageResponse("đại lý",null).UPDATE_SUCCESSFULLY);
                                                }
                                            });
                                        }
                                    });
                        //         }
                        //     }
                        // });
                    }
                }
            });
        }
    });
}