const express = require("express");
const router = express.Router();
const moment = require("moment");
const async = require("async");
const md5 = require("md5");
const _ = require("lodash");

const messageValidate = require("./../utilities/MessageValidate");
const messageResponse = require("./../utilities/MessageResponse");
const constants =  require("./../utilities/Constants");

const Package = require("./../model/Package");
const AgencyPackage = require("./../model/AgencyPackage");

exports.create = (req, res) =>{
    req.check("package_name", messageValidate("package_name")).notEmpty();
    req.check("plan_id", messageValidate("plan_id")).notEmpty();
    req.check("package_price", messageValidate("package_price")).notEmpty();
    req.check("package_description", messageValidate("package_description")).notEmpty();
    req.check("package_status", messageValidate("package_status")).notEmpty();
    req.check("user_create", messageValidate("user_create")).notEmpty();

    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let packageName = req.body.package_name;
            let planId = req.body.plan_id;
            Package.checkPackageForCreate(packageName, planId, (err, existModule) => {                
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);        
                }else {
                    if(existModule){
                        res.json(messageResponse("Tên gói và plan id", null).EXIST_OBJECT);
                    }else { 
                        let now = new Date();
                        let newPackage = new Package({
                            "package_name": packageName,
                            "plan_id": planId,
                            "package_price": req.body.package_price,
                            "package_description": req.body.package_description,
                            "package_status": req.body.package_status,
                            "user_create": req.body.user_create,
                            "day_create_date": now,
                            "day_create_string": moment(now).format(constants.FORMAT_FULL_DATE),
                        });
                        Package.insertPackage(newPackage, (err, data) => {
                            if(err){
                                res.json(messageResponse(err).QUERY_ERROR);
                            }else{                            
                                res.json(messageResponse("gói", data).ADD_SUCCESSFULLY);   
                            }
                        });
                    }
                }
            });
        }
    });
}

exports.getPackageByPackageName = (req, res) => {
    req.check("package_name", messageValidate("package_name")).notEmpty();
    req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            Package.takePackageByPackageName(req.body.package_name, (err, package)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(package){
                        res.json(messageResponse("", package).FOUND_DATA);
                    }else {
                        res.json(messageResponse("", "").NOT_FOUND)
                    }
                }
            });
        }
    });
}

exports.getPackageByPlanId = (req, res) => {
    req.check("plan_id", messageValidate("plan_id")).notEmpty();
    req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            Package.takePackageByPlanId(req.body.plan_id, (err, package)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(package){
                        res.json(messageResponse("", package).FOUND_DATA);
                    }else {
                        res.json(messageResponse("", "").NOT_FOUND)
                    }
                    
                }
            });
        }
    });
}

exports.getBySkipLimit = (req,res) => {
    req.check("skip", messageValidate("skip")).notEmpty();
    req.check("limit", messageValidate("limit")).notEmpty();
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.json(messageResponse("", result.array()).MISSING_FIELD);
        } else {
            Package.sum((err, totalPackages) => {
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(totalPackages.length == 0){
                        res.json(messageResponse(null, null).NOT_FOUND);
                    }else{
                        Package.getBySkipLimit(req.query.skip, req.query.limit, (err, listPackage) => {
                            if(err){
                                res.json(messageResponse(err).QUERY_ERROR);
                            }else{
                                res.json(messageResponse(null, listPackage, totalPackages).FOUND_DATA);
                            }
                        });
                    }
                }
            });
        }
    }).catch(function(rea){
        console.log(rea);
    });
}

exports.getByDataSearchSkipLimit = (req, res) => {
	req.check("limit", messageValidate("limit")).notEmpty();
    req.check("skip", messageValidate("skip")).notEmpty();
    req.check("packageName", messageValidate("packageName")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let dataSearch = decodeURI(req.query.packageName);
            Package.getByDataSearchSkipLimit(req.query.skip, req.query.limit, dataSearch, (err, dataList)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{                    
                    Package.sumQuery(dataSearch, (err, data)=>{
                        if(err){
                            res.json(messageResponse(err).QUERY_ERROR);
                        }else{
                            res.json(messageResponse(null, dataList, data).FOUND_DATA);
                        }
                    });
                }
            });
    	}
    });
}

exports.getPackageById = (req, res) => {
    req.check("package_id", messageValidate("package_id")).notEmpty();
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.json(messageResponse("", result.array()).MISSING_FIELD);
        } else {
            Package.takePackageById(req.query.package_id, (err, package) => {
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(!package){
                        return res.json(messageResponse(null, null).NOT_FOUND);
                    }else{
                        res.json(messageResponse(null, package, null).FOUND_DATA);                       
                    }
                }
            });
        }
    }).catch(function(rea){
        console.log(rea);
    });
}

exports.getPackageByIdAndPlanId = (req, res) => {
    req.check("package_id", messageValidate("package_id")).notEmpty();
    req.check("plan_id", messageValidate("plan_id")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            Package.checkPackageByPlanId(req.body.package_id, req.body.plan_id,(err, package)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(package){
                        res.json(messageResponse("", package).FOUND_DATA);
                    }else {
                        res.json(messageResponse("", "").NOT_FOUND)
                    }
                }
            });
    	}
    });
}

exports.getPackageByIdAndPackageName = (req, res) => {
    req.check("package_id", messageValidate("package_id")).notEmpty();
    req.check("package_name", messageValidate("package_name")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            Package.checkPackageByPackageName(req.body.package_id, req.body.package_name,(err, package)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(package){
                        res.json(messageResponse("", package).FOUND_DATA);
                    }else {
                        res.json(messageResponse("", "").NOT_FOUND)
                    }
                }
            });
    	}
    });
}

exports.update = (req, res) => {
    req.check("package_id", messageValidate("package_id")).notEmpty();
    req.check("package_name", messageValidate("package_name")).notEmpty();
    req.check("plan_id", messageValidate("plan_id")).notEmpty();
    req.check("package_price", messageValidate("package_price")).notEmpty();
    req.check("package_description", messageValidate("package_description")).notEmpty();
    req.check("package_status", messageValidate("package_status")).notEmpty();
    req.check("user_update", messageValidate("user_update")).notEmpty();
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
            res.json(messageResponse("", result.array()).MISSING_FIELD);
        } else {
            let packageId = req.body.package_id;
            let packageName = req.body.package_name;
            let planId = req.body.plan_id;
            Package.checkPackageForUpdate(packageId, packageName, planId, (err, existPackage) => {                
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);        
                }else {
                    if(existPackage){
                        res.json(messageResponse("Tên gói và plan id", null).EXIST_OBJECT);
                    }else {        
                        let now = new Date();
                        let dataUpdate = {
                            "package_name": packageName,
                            "plan_id": planId,
                            "package_price": req.body.package_price,
                            "package_description": req.body.package_description,
                            "package_status": req.body.package_status,
                            "day_update_date": now,                        
                            "day_update_string": moment(now).format(constants.FORMAT_FULL_DATE),
                            "user_update": req.body.user_update
                        }  
                        Package.updateById(packageId, dataUpdate,(err,raw)=>{ 
                            if(err){
                                res.json(messageResponse(err).QUERY_ERROR);
                            }else{
                                res.json(messageResponse("gói", null).UPDATE_SUCCESSFULLY);
                            }
                        });
                    }               
                }
            });
        }
    })   
}

exports.remove = (req,res) => {
    req.check("package_id", messageValidate("package_id")).notEmpty();
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
            res.json(messageResponse("", result.array()).MISSING_FIELD);
        } else {
            let packageId = req.body.package_id;
            AgencyPackage.takeAgencyPackageByPackage(packageId, (err, agencyPackage)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else {
                    if(agencyPackage){
                        res.json(messageResponse("Không thể xóa vì có tồn tại phân quyền", null).CUSTOM_MESSAGE);   
                    }else {
                        Package.deleteById(packageId, (err)=>{
                            if(err){
                                res.json(messageResponse(null,null).DELETE_FAIL);
                            }else {                
                                res.json(messageResponse(null, null).DELETE_SUCCESSFULLY);
                            }
                        });
                    }
                }
            });
        }
    }).catch(function(rea){
        res.json(rea);
    });   
}