const async = require("async");
const _ = require("lodash");
const moment = require("moment");

const AgencyPackage = require("./../model/AgencyPackage");
const Profile = require("./../model/Profile");
const GiftCode = require("./../model/GiftCode");
const Agency = require("./../model/Agency");

const config = require("../config/" + (process.env["NODE_ENV"] || "production"));
const constants =  require("./../utilities/Constants");
const messageValidate = require("./../utilities/MessageValidate");
const messageResponse = require("./../utilities/MessageResponse");

exports.getBySkipLimit = (req,res) => {
    req.check("skip", messageValidate("skip")).notEmpty();
    req.check("limit", messageValidate("limit")).notEmpty();
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.json(messageResponse("", result.array()).MISSING_FIELD);
        } else {
            GiftCode.sum((err, totalData) => {
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(totalData.length == 0){
                        res.json(messageResponse(null, null).NOT_FOUND);
                    }else{
                        GiftCode.getAll(req.query.skip, req.query.limit, (err, giftCode) => {
                            if(err){
                                res.json(messageResponse(err).QUERY_ERROR);
                            }else{
                                res.json(messageResponse(null, giftCode, totalData).FOUND_DATA);
                            }
                        });
                    }
                }
            });
        }
    }).catch(function(rea){
        console.log(rea);
    });
}

exports.getByDataSearchSkipLimit = (req, res) => {
	req.check("limit", messageValidate("limit")).notEmpty();
    req.check("skip", messageValidate("skip")).notEmpty();
    req.check("code_id", messageValidate("code_id")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let dataSearch = decodeURI(req.query.code_id);
            GiftCode.takeDataSearchSkipLimit(req.query.skip, req.query.limit, dataSearch, (err, dataList)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{                    
                    GiftCode.sumQuery(dataSearch, (err, data)=>{
                        if(err){
                            res.json(messageResponse(err).QUERY_ERROR);
                        }else{
                            res.json(messageResponse(null, dataList, data).FOUND_DATA);
                        }
                    });
                }
            });
    	}
    });
}

exports.updateActiveStatus = (req, res) => {
    req.check("code_id", messageValidate("code_id")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let dataUpdate = {
                "active_status": true
            }
            let codeId = req.body.code_id;
            GiftCode.updateById(codeId, dataUpdate, (err, status) => {
                if(err){
                    console.log("Update status code unsuccessful " + codeId);
                }
            });
            res.json(messageResponse("Tiếp nhận thành công", null, null).CUSTOM_MESSAGE);
    	}
    });
}