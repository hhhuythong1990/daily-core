const express = require("express");
const router = express.Router();

const async = require("async");

const messageValidate = require("./../utilities/MessageValidate");
const messageResponse = require("./../utilities/MessageResponse");
const constants =  require("./../utilities/Constants");

const RequestGiftCode = require("./../model/RequestGiftCode");

exports.getAllRequestCode = (req, res) => {
	req.check("limit", messageValidate("limit")).notEmpty();
    req.check("skip", messageValidate("skip")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let dataSearch = req.body.data_search;
            let dataSkip = req.body.skip;
            let dataLimit = req.body.limit;            
            dataQuery(dataSkip, dataLimit, dataSearch, (err, dataList)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    let dataResponse = {
                        listPaid: dataList.listPaid,
                        sumPaid: dataList.sumPaid
                    }
                    res.json(messageResponse(null, dataResponse, dataList.dataCount).FOUND_DATA);
                }
            });
    	}
    });
}

dataQuery = (dataSkip, dataLimit, dataSearch, callback) => {
    async.parallel({
        listPaid: function (callback) {
            RequestGiftCode.takeDataSearchSkipLimit(dataSkip, dataLimit, dataSearch, callback);
        },
        dataCount: function (callback) {
            RequestGiftCode.sumQuery(dataSearch, callback)
        },
        sumPaid: function (callback) {
            RequestGiftCode.sumPaid(dataSearch, callback);
        }
    }, function (err, results) {
        if(err){
            callback(err, null);
        }else{
            callback(null, results);
        }
        
    });
}

exports.exportRequestCode = (req, res) => {    
    RequestGiftCode.takeDataSearch(req.body.data_search, (err, listData) => {
        if(err){
            res.json(messageResponse(err).QUERY_ERROR);
        }else{
            res.json(messageResponse(null, listData, null).FOUND_DATA);
        }
    })
}