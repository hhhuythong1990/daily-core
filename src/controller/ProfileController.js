const express = require("express");
const router = express.Router();
const moment = require("moment");
const async = require("async");
const md5 = require("md5");
const _ = require("lodash");

const messageValidate = require("./../utilities/MessageValidate");
const messageResponse = require("./../utilities/MessageResponse");
const constants =  require("./../utilities/Constants");

const Profile = require("./../model/Profile");
const GroupModule = require("./../model/GroupModule");
const GroupUser = require("./../model/GroupUser");
const Permission = require("./../model/Permission");
const Agency = require("./../model/Agency");
const ModulePermission = require("./../model/ModulePermission");
const AgencyPackage = require("./../model/AgencyPackage");

function updateOnline(profileId, statusOnline, timeOnline) {
    let dataUpdate = {
        "date_online": timeOnline,
        "is_online": statusOnline
    };
    Profile.updateByeId(profileId, dataUpdate, (err)=>{
        if(err){
            console.log(err)
        }
    });
}
exports.create = (req, res) =>{
    req.check("full_name", messageValidate("full_name")).notEmpty();
    req.check("user_name", messageValidate("user_name")).notEmpty();
    req.check("password", messageValidate("password")).notEmpty();
    req.check("permissions", messageValidate("permissions")).notEmpty();
    req.check("group_user", messageValidate("group_user")).notEmpty();
    req.check("publish", messageValidate("publish")).notEmpty();
    req.check("user_create", messageValidate("user_create")).notEmpty();
    
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let userName = req.body.user_name;
            Profile.takeProfileByUserName(userName, (err, profile)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(profile){
                        res.json(messageResponse("Tên đăng nhập", null).EXIST_OBJECT);
                    }else {
                        let now = new Date();
                        let newProfile = new Profile({
                            "full_name":  req.body.full_name,
                            "user_name":  userName,
                            "password":  md5(req.body.password + constants.SALT),
                            "is_publish": req.body.publish,
                            "permissions": req.body.permissions,
                            "group_user": req.body.group_user,
                            "group_parent": req.body.group_parent,
                            "group_modules": req.body.group_modules,
                            "user_create": req.body.user_create,
                            "day_create_date": now,
                            "day_create_string": moment(now).format(constants.FORMAT_FULL_DATE),
                        });
                        Profile.insertProfile(newProfile,(err,data) => {
                            if(err){
                                res.json(messageResponse(err).QUERY_ERROR);
                            }else{
                                res.json(messageResponse("hồ sơ", data).ADD_SUCCESSFULLY);   
                            }
                        });
                    }
                }
            });
        }
    });
}

exports.getAll = (req, res) => {
	req.check("limit", messageValidate("limit")).notEmpty();
    req.check("skip", messageValidate("skip")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            Profile.getAll(req.query.skip, req.query.limit,(err,datalist)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    Profile.sum((err,data)=>{
                        if(err){
                            res.json(messageResponse(err).QUERY_ERROR);
                        }else{
                            res.json(messageResponse(null, datalist, data).FOUND_DATA);
                        }
                    });
                }
            });
    	}
    });
}

exports.getByDataSearchSkipLimit = (req, res) => {
	req.check("limit", messageValidate("limit")).notEmpty();
    req.check("skip", messageValidate("skip")).notEmpty();
    req.check("userName", messageValidate("userName")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let dataSearch = decodeURI(req.query.userName);
            Profile.takeDataSearchSkipLimit(req.query.skip, req.query.limit, dataSearch, (err, datalist)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    Profile.sumQuery(dataSearch, (err,data)=>{
                        if(err){
                            res.json(messageResponse(err).QUERY_ERROR);
                        }else{
                            res.json(messageResponse(null, datalist, data).FOUND_DATA);
                        }
                    });
                }
            });
    	}
    });
}

exports.remove = (req, res) => {
	req.check("profile_id", messageValidate("profile_id")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    	    res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let profileId = req.body.profile_id;
            Profile.takeProfileById(profileId, (err, profileInfo)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    let profileId = profileInfo._id;
                    let agencyId = profileInfo.is_agency;
                    if(agencyId){
                        AgencyPackage.takeAgencyPackageByAgency(profileId, (err, agencyPackage) => {
                            if(err){
                                res.json(messageResponse(err).QUERY_ERROR);
                            }else {
                                if(agencyPackage){
                                    res.json(messageResponse("Không thể xóa vì có tồn tại phân quyền", null).CUSTOM_MESSAGE);
                                }else {
                                    Profile.deleteById(profileId,(err)=>{
                                        if(err){
                                            res.json(messageResponse(err).QUERY_ERROR);
                                        }else { 
                                            if(agencyId){
                                                Agency.deleteById(agencyId, (err)=>{
                                                    if(err){
                                                        res.json(messageResponse(err).QUERY_ERROR);
                                                    }else {
                                                        res.json(messageResponse(null, null).DELETE_SUCCESSFULLY);
                                                    }
                                                });
                                            }else {
                                                res.json(messageResponse(null, null).DELETE_SUCCESSFULLY);
                                            }
                                        }
                                    });
                                }
                            }
                        }); 
                    }
                }
            })
    	}
    });
}

exports.update = (req, res) => {
	req.check("profile_id", messageValidate("profile_id")).notEmpty();
	req.check("full_name", messageValidate("full_name")).notEmpty();
    req.check("user_name", messageValidate("user_name")).notEmpty();
    req.check("permissions", messageValidate("permissions")).notEmpty();
    req.check("group_modules", messageValidate("group_modules")).notEmpty();
    req.check("group_user", messageValidate("group_user")).notEmpty();
    req.check("publish", messageValidate("publish")).notEmpty();
    req.check("user_update", messageValidate("user_update")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let profileId = req.body.profile_id;
            let userName = req.body.user_name;
            Profile.takeProfileByIdAndUserName(profileId, userName, (err, profile)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(profile){
                        res.json(messageResponse("Tên đăng nhập", null).EXIST_OBJECT);
                    }else {
                        let now = new Date();
                        let password = req.body.password;
                        let dataUpdate = {
                            "full_name":  req.body.full_name,
                            "user_name":  userName,
                            "permissions": req.body.permissions,
                            "group_user": req.body.group_user,
                            "group_parent": req.body.group_parent,
                            "group_modules": req.body.group_modules,
                            "is_publish": req.body.publish,
                            "is_changed": true,
                            "user_update"  : req.body.user_update,
                            "day_update_date"  : now,
                            "day_update_string": moment(now).format(constants.FORMAT_FULL_DATE),
                        };
                        
                        if (password !== "") {
                            dataUpdate["password"] = md5(password + constants.SALT);
                        }
                        Profile.updateByeId(profileId, dataUpdate, (err, result)=>{
                            if(err){
                                res.json(messageResponse(err).QUERY_ERROR);
                            } else{
                                res.json(messageResponse("người dùng",null).UPDATE_SUCCESSFULLY);
                            }
                        });
                    }
                }
            });
		}
	});
}

exports.authenticationUserNamePassword = (req, res) => {
    req.check("user_name", messageValidate("user_name")).notEmpty();
	req.check("password", messageValidate("password")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            
			Profile.takeProfileByUsername(req.body.user_name, (err, profile)=>{
				if(err){
					res.json(messageResponse(err).QUERY_ERROR);
				}else{                    
                    if(!profile){
                        res.json(messageResponse(null, null).LOGIN_INCORRECT);
                    }else{                        
                        // let password = md5(req.body.password + constants.SALT);
                        // if(profile.password == password){
                        //     res.json(messageResponse(null, profile).LOGIN_SUCCESSFULLY);
                        // }else{
                        //     res.json(messageResponse(null, null).LOGIN_INCORRECT);
                        // }

                        if(profile.is_locked == true){
                            res.json(messageResponse("Rất tiếc tài khoản của bạn đã bị khóa", null).CUSTOM_MESSAGE);
                        }else {
                            let password = md5(req.body.password + constants.SALT);
                            let now = new Date();
                            let expireTime = moment(now, "DD-MM-YYYY H:mm").add(2, 'H').valueOf();
                            if(profile.password == password){
                                if(profile.is_online == true){
                                    if(profile.date_online <= now.getTime()){
                                        updateOnline(profile._id, true, expireTime);
                                        res.json(messageResponse(null, profile).LOGIN_SUCCESSFULLY);
                                    }else {
                                        res.json(messageResponse("Tài khoản đã được đăng nhập", null).CUSTOM_MESSAGE);
                                    }
                                }else {
                                    updateOnline(profile._id, true, expireTime);
                                    res.json(messageResponse(null, profile).LOGIN_SUCCESSFULLY);
                                }
                            }else{
                                res.json(messageResponse(null, null).LOGIN_INCORRECT);
                            }
                        }
                    }
				}
			})
    	}
    });
}

exports.getProfileByProfileId = (req, res) => {
    req.check("profile_id", messageValidate("profile_id")).notEmpty();
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.json(messageResponse("", result.array()).MISSING_FIELD);
        } else {
            let profileId = req.query.profile_id;
            
            async.parallel({
                collection: function (callback) {
                    collectionData(callback);
                },
                profile: function (callback) {
                    Profile.takeModuleByProfileId(profileId, callback);
                },
            }, function (err, results) {
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    let temp_group_modules = _.map(results.profile.group_modules, (value) => {
                        return String(value);
                    });
                    _.each(results.collection.groupModules, (groupModule) => {
                        if (_.includes(temp_group_modules, String(groupModule._id))) {
                            groupModule.temp = "selected";
                        } else {
                            groupModule.temp = "";
                        }
                    });

                    let temp_permission = _.map(results.profile.permissions, (value) => {
                        return String(value);
                    });                    
                    _.each(results.collection.permissions, (permission) => {
                        if (_.includes(temp_permission, String(permission._id))) {
                            permission.temp = "selected";
                        } else {
                            permission.temp = "";
                        }
                    });

                    let temp_group_user = String(results.profile.group_user);                    
                    _.each(results.collection.groupUsers, (groupUser) => {
                        if (_.includes(temp_group_user, String(groupUser._id))) {
                            groupUser.temp = "selected";
                        } else {
                            groupUser.temp = "";
                        }
                    });
                    res.json(messageResponse("", results).FOUND_DATA);
                }
                
            });
        }
    }).catch(function(rea){
        console.log(rea);
    });
}

exports.getDataRelationProfile = (req, res) => {
    collectionData ((err, results) => {
        if(err){
            res.json(messageResponse(err).QUERY_ERROR);
        }else{
            res.json(messageResponse("", results).FOUND_DATA);
        }
    });
}

collectionData = (callback) => {
    async.parallel({        
        groupUsers: function (callback) {
            getGroupUser(callback);
        },
        groupModules: function (callback) {
            getProfileGroupModule(callback);
        },
        permissions: function (callback) {
            getProfilePermissions(callback);
        },
    }, function (err, results) {
        if(err){
            callback(err, null);
        }else{
            callback(null, results);
        }
        
    });
}

getProfileGroupModule = (callback) => {
    GroupModule.takeAllGroupModule((err, groupModules) => {
		if(err){
			callback(err, null);
		}else { 
			callback(null, groupModules);
		}
	});
}

getGroupUser = (callback) => {
    GroupUser.takeAllGroupUser((err, groupUsers) => {
        if(err){
			callback(err, null);
		}else {
			callback(null, groupUsers);
		}
    });
}

getProfilePermissions = (callback) => {
    // Permission.getAllPermission((err, permissions) => {
	// 	if(err){
	// 		callback(err, null);
	// 	}else { 
	// 		callback(null, permissions);
	// 	}
    // });
    
    ModulePermission.takeModulePermissionAll((err, permissions) => {
		if(err){
			callback(err, null);
		}else { 
			callback(null, permissions);
		}
	});
}

exports.getProfileByFullName = (req, res) => {
    req.check("full_name", messageValidate("full_name")).notEmpty();
    req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            Profile.takeProfileByFullName(req.body.full_name, (err, profile)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(profile){
                        res.json(messageResponse("", profile).FOUND_DATA);
                    }else {
                        res.json(messageResponse("", "").NOT_FOUND)
                    }
                    
                }
            });
        }
    });
}

exports.getProfileByUserName = (req, res) => {
    req.check("user_name", messageValidate("user_name")).notEmpty();
    req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            Profile.takeProfileByUserName(req.body.user_name, (err, profile)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(profile){
                        res.json(messageResponse("", profile).FOUND_DATA);
                    }else {
                        res.json(messageResponse("", "").NOT_FOUND)
                    }
                    
                }
            });
        }
    });
}

exports.getProfileByIdAndUserName = (req, res) => {
    req.check("user_id", messageValidate("user_id")).notEmpty();
    req.check("user_name", messageValidate("user_name")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            Profile.takeProfileByIdAndUserName(req.body.user_id, req.body.user_name,(err, profile)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(profile){
                        res.json(messageResponse("", profile).FOUND_DATA);
                    }else {
                        res.json(messageResponse("", "").NOT_FOUND)
                    }
                }
            });
    	}
    });
}

exports.changePassword = (req, res) => {
    req.check("user_id", messageValidate("user_id")).notEmpty();
    req.check("old_password", messageValidate("old_password")).notEmpty();
    req.check("new_password", messageValidate("new_password")).notEmpty();
    req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let profileId = req.body.user_id;
            Profile.takeProfileById(profileId, (err, profile)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(profile){
                        let oldPassword = md5(req.body.old_password + constants.SALT);
                        if(profile.password == oldPassword){
                            let dataUpdate = {
                                "password": md5(req.body.new_password + constants.SALT)
                            };
                            Profile.updateByeId(profileId, dataUpdate, (err, result)=>{
                                if(err){
                                    res.json(messageResponse(err).QUERY_ERROR);
                                } else{
                                    res.json(messageResponse("cập nhật mật khẩu", null).UPDATE_SUCCESSFULLY);
                                }
                            });
                        }else{
                            res.json(messageResponse("mật khẩu cũ không đúng", null).CUSTOM_MESSAGE);
                        }
                    }else {
                        res.json(messageResponse("", "").NOT_FOUND)
                    }
                }
            });
    	}
    });
}   

exports.changeOnline = (req, res) => {
    req.check("profile_id", messageValidate("profile_id")).notEmpty();
    req.check("is_online", messageValidate("is_online")).notEmpty();
    req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
            
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            
            let profileId = req.body.profile_id;
            let dataUpdate = {
                "is_online":  (req.body.is_online == "true")?true:false,
            };
            Profile.updateByeId(profileId, dataUpdate, (err, result)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                } else{
                    res.json(messageResponse("người dùng",null).UPDATE_SUCCESSFULLY);
                }
            });
        }
    });
}

exports.changeBlock = (req, res) => {
    req.check("profile_id", messageValidate("profile_id")).notEmpty();
    req.check("is_locked", messageValidate("is_locked")).notEmpty();
    req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
            
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let profileId = req.body.profile_id;
            let lockStatus = req.body.is_locked;
            let dataUpdate = {
                "is_locked":  (lockStatus == "true")?true:false,
                
            };
            if(lockStatus == "true"){
                dataUpdate["is_changed"] = true;
            }
            Profile.updateByeId(profileId, dataUpdate, (err, result)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                } else{
                    res.json(messageResponse("người dùng",null).UPDATE_SUCCESSFULLY);
                }
            });
        }
    });
}

exports.logout = (req, res) => {
    req.check("profile_id", messageValidate("profile_id")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            updateOnline(req.body.profile_id, false, 0);
            res.json(messageResponse(null, null).CUSTOM_MESSAGE);
        }
    });
}

exports.takeProfileChange = (req, res) => {
    req.check("profile_id", messageValidate("profile_id")).notEmpty();
    req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let profileId = req.body.profile_id;
            Profile.takeProfileChange(profileId, true, (err, profileChanged)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                } else{
                    if(!profileChanged){
                        res.json(messageResponse(null, null).CHECK_CHANGED);
                    }else {
                        if(profileChanged.is_locked == true){
                            let dataResponse = {
                                "locked": true,
                                "dataUpdate": null
                            }
                            updateChangedProfile(profileId);
                            return res.json(messageResponse(null, dataResponse).CHECK_CHANGED);
                        }else {
                            Profile.takeProfileByIdWithData(profileId, (err, profileInfo)=>{
                                if(err){
                                    res.json(messageResponse(err).QUERY_ERROR);
                                }else {
                                    let dataResponse = {
                                        "locked": null,
                                        "dataUpdate": profileInfo
                                    }
                                    updateChangedProfile(profileId);
                                    return res.json(messageResponse(null, dataResponse).CHECK_CHANGED);
                                }
                            });
                        }
                    }
                }
            });
        }
    });
}

function updateChangedProfile(profileId){
    let dataUpdate = {
        "is_changed": true,
    };
    Profile.updateByeId(profileId, dataUpdate);
}