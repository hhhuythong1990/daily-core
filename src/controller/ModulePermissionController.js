const moment = require("moment");
const async = require("async");

const messageValidate = require("./../utilities/MessageValidate");
const messageResponse = require("./../utilities/MessageResponse");
const constants =  require("./../utilities/Constants");

const ModulePermission = require("./../model/ModulePermission");
const Module = require("./../model/Module");
const Permission = require("./../model/Permission");

exports.create = (req, res) => {
	req.check("module_permission", messageValidate("module_permission")).notEmpty();
	req.check("module", messageValidate("module")).notEmpty();
	req.check("permissions", messageValidate("permissions")).notEmpty();
	req.check("publish", messageValidate("publish")).notEmpty();
	req.check("user_create", messageValidate("user_create")).notEmpty();
    req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
			let moduleName = req.body.module_permission;
			ModulePermission.takeModulePermissionByName(moduleName, (err, modulePermission) => {
				if(err){
					res.json(messageResponse(err).QUERY_ERROR);
				}else {
					if(modulePermission){
						res.json(messageResponse("Mô-dun và quền hạn", modulePermission).EXIST_OBJECT);
					}else {
						let now = new Date();
						let newModulePermission = new ModulePermission({
							"module_permission_name": moduleName,
							"module": req.body.module,
							"permissions" : req.body.permissions,
							"user_create": req.body.user_create,
							"is_publish": req.body.publish,
							"day_create_date": now,
							"day_create_string": moment(now).format(constants.FORMAT_FULL_DATE)
						});
						ModulePermission.insertModulePermission(newModulePermission,(err, resp)=>{
							if(err){
								res.json(messageResponse(err).QUERY_ERROR);
							} else {
								res.json(messageResponse("mô-dun và quền hạn", resp).ADD_SUCCESSFULLY);    
							}
						});
					}
				}
			});
    	}
    });
}

exports.getBySkipLimit = (req, res) => {
	req.check("limit", messageValidate("limit")).notEmpty();
	req.check("skip", messageValidate("skip")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
			ModulePermission.takeModulePermissionBySkipLimit(req.query.skip, req.query.limit,(err, datalist)=>{
				if(err){
					res.json(messageResponse(err).QUERY_ERROR);
				}else{
					ModulePermission.sumModulePermission((err,data)=>{
						if(err){
							res.json(messageResponse(err).QUERY_ERROR);
						}else{
							res.json(messageResponse(null, datalist, data).FOUND_DATA);
						}
					});
				}
			});
    	}
    });
}

exports.getDataModulePermission = (req, res) => {
    collectionDataModulePermission ((err, results) => {
        if(err){
            res.json(messageResponse(err).QUERY_ERROR);
        }else{
            res.json(messageResponse("", results).FOUND_DATA);
        }
    });
}

collectionDataModulePermission = (callback) => {
    async.parallel({
        modules: function (callback) {
            getModulePermissionModules(callback);
        },
        permissions: function (callback) {
            getModulePermissionPermissions(callback);
        },
    }, function (err, results) {
        if(err){
            callback(err, null);
        }else{
            callback(null, results);
        }
        
    });
}

getModulePermissionPermissions = (callback) => {
    Permission.getAllPermission((err, permissions) => {
		if(err){
			callback(err, null);
		}else {
			callback(null, permissions);
		}
	});
}

getModulePermissionModules = (callback) => {
    Module.takeAllModule((err, modules) => {
		if(err){
			callback(err, null);
		}else {
			callback(null, modules);
		}
	});
}

exports.remove = (req, res) => {
	req.check("module_permission_id", messageValidate("module_permission_id")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
			ModulePermission.deleteById(req.body.module_permission_id,(err)=>{
				if(err){
					res.json(messageResponse(err).QUERY_ERROR);
				}else{ 
					res.json(messageResponse(null, null).DELETE_SUCCESSFULLY);
				}
			})
    	}
    });
}

exports.getModulePermissionById = (req, res) => {
    req.check("module_permission_id", messageValidate("module_permission_id")).notEmpty();
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.json(messageResponse("", result.array()).MISSING_FIELD);
        } else {
            ModulePermission.takeModulePermissionById(req.query.module_permission_id, (err, modulePermission) => {
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(!modulePermission){
                        return res.json(messageResponse(null, null).NOT_FOUND);
                    }else{
                        res.json(messageResponse(null, modulePermission, null).FOUND_DATA);                       
                    }
                }
            });
        }
    }).catch(function(rea){
        console.log(rea);
    });
}
exports.update = (req, res) => {
	req.check("module_permission", messageValidate("module_permission")).notEmpty();
	req.check("id", messageValidate("id")).notEmpty();
	req.check("permissions", messageValidate("permissions")).notEmpty();
	req.check("publish", messageValidate("publish")).notEmpty();
	req.check("user_update", messageValidate("user_update")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
			let moduleName = req.body.module_permission;
			let id = req.body.id;
			ModulePermission.takeModulePermissionByIdAndName(id, moduleName, (err, modulePermission) => {
				if(err){
					res.json(messageResponse(err).QUERY_ERROR);
				}else {
					if(modulePermission){
						res.json(messageResponse("Mô-dun và quền hạn", modulePermission).EXIST_OBJECT);
					}else {
						let now = new Date();
						let dataUpdate = {
							"module_permission_name": moduleName,
							"permissions": req.body.permissions,
							"is_publish": req.body.publish,
							"user_update": req.body.user_update,
							"day_update_date": now,                        
							"day_update_string": moment(now).format(constants.FORMAT_FULL_DATE),
						};
						ModulePermission.updateByeId(id, dataUpdate, (err)=>{
							if(err){
								res.json(messageResponse(err).QUERY_ERROR);
							}else{
								res.json(messageResponse("mô-đun và quyền hạn", null).UPDATE_SUCCESSFULLY);
							}
						});
					}
				}
			});
    	}
    });
}