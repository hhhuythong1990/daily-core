const express = require("express");
const router = express.Router();
const moment = require("moment");
const async = require("async");
const md5 = require("md5");
const _ = require("lodash");

const messageValidate = require("./../utilities/MessageValidate");
const messageResponse = require("./../utilities/MessageResponse");
const constants =  require("./../utilities/Constants");

const Profile = require("./../model/Profile");
const Package = require("./../model/Package");
const Agency = require("./../model/Agency");
const AgencyPackage = require("./../model/AgencyPackage");
const GiftCode = require("./../model/GiftCode");

exports.create = (req, res) =>{
    req.check("packages", messageValidate("packages")).notEmpty();
    req.check("user_create", messageValidate("user_create")).notEmpty();    
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let agencyId = req.body.agency;
            let packages = req.body.packages;
            
            if(packages.length == 1){
                let packageItem = packages[0];
                let packageId = packageItem.package;
                AgencyPackage.takeAgencyPackageByAgencyAndPackage(agencyId, packageId, (err, agencyPackage)=>{
                    if(err){
                        res.json(messageResponse(err).QUERY_ERROR);
                    }else{
                        if(agencyPackage){
                            res.json(messageResponse("Phân quyền bán gói", null).EXIST_OBJECT);
                        }else {
                            Package.takePackageById(packageId, (err, packageInfo) => {
                                if(err){
                                    res.json(messageResponse(err).QUERY_ERROR);
                                }else{
                                    if(packageInfo){
                                        let packagePrice = packageInfo.package_price;
                                        let cycle = packageItem.cycle;
                                        let now = new Date();
                                        let newAgencyPackage = new AgencyPackage({
                                            "agency": agencyId,
                                            "agency_package_name": packageItem.agency_package_name,
                                            "package": packageId,
                                            "cycle": parseFloat(packageItem.cycle),
                                            "price_after_cycle": calCycle(packagePrice, packageItem.cycle),
                                            "agency_package_status": packageItem.agency_package_status,
                                            "user_create": req.body.user_create,
                                            "day_create_date": now,
                                            "day_create_string": moment(now).format(constants.FORMAT_FULL_DATE),
                                        });
                                        AgencyPackage.insertAgencyPackage(newAgencyPackage, (err, data) => {
                                            if(err){
                                                res.json(messageResponse(err).QUERY_ERROR);
                                            }else{
                                                res.json(messageResponse("phân quyền bán gói", null).ADD_SUCCESSFULLY);
                                            }
                                        });
                                    }else {
                                        res.json(messageResponse("gói", null).NOT_EXIST_OBJECT);
                                    }
                                }
                            });
                        }
                    }
                });
            }else {
                async.each(packages, function(element, cb) {
                    async.parallel([
                        function(callback){
                            let packageId = element.package;
                            AgencyPackage.takeAgencyPackageByAgencyAndPackage(agencyId, packageId, (err, agencyPackage)=>{
                                if(err){
                                    callback(err);
                                }else{
                                    if(!agencyPackage){
                                        Package.takePackageById(packageId, (err, packageInfo) => {
                                            if(err){
                                                callback(err);
                                            }else{
                                                if(packageInfo){
                                                    let packagePrice = packageInfo.package_price;
                                                    let cycle = element.cycle;
                                                    let now = new Date();
                                                    let newAgencyPackage = new AgencyPackage({
                                                        "agency": agencyId,
                                                        "agency_package_name": element.agency_package_name,
                                                        "package": packageId,
                                                        "cycle": parseFloat(element.cycle),
                                                        "price_after_cycle": calCycle(packagePrice, element.cycle),
                                                        "agency_package_status": element.agency_package_status,
                                                        "user_create": req.body.user_create,
                                                        "day_create_date": now,
                                                        "day_create_string": moment(now).format(constants.FORMAT_FULL_DATE),
                                                    });
                                                    AgencyPackage.insertAgencyPackage(newAgencyPackage, (err, data) => {
                                                        if(err){
                                                            callback(err);
                                                        }else{
                                                            callback(null);
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    }
                                }
                            })
                        }
                    ],cb());
                }, function(){
                    res.json(messageResponse("phân quyền bán gói", null).ADD_SUCCESSFULLY);    
                });
            }
        }
    });
}
exports.getDataRelation = (req, res) =>{
    collectionAgencyPackage ((err, results) => {
        if(err){
            res.json(messageResponse(err).QUERY_ERROR);
        }else{
            res.json(messageResponse("", results).FOUND_DATA);
        }
    });
}

collectionAgencyPackage = (callback) => {
    async.parallel({
        getAgency: function (callback) {
            getAgency(callback);
        },
        getPackage: function (callback) {
            getPackage(callback);
        }
    }, function (err, results) {
        if(err){
            callback(err, null);
        }else{
            callback(null, results);
        }
        
    });
}
getAgency = (callback) => {
    Profile.takeAllAgency((err, agencies) => {
		if(err){
			callback(err, null);
		}else { 
			callback(null, agencies);
		}
	});
}

getPackage = (callback) => {
    Package.takeAllPackageActive((err, packages) => {
        if(err){
			callback(err, null);
		}else { 
			callback(null, packages);
		}
    });
}

exports.getBySkipLimit = (req,res) => {
    req.check("skip", messageValidate("skip")).notEmpty();
    req.check("limit", messageValidate("limit")).notEmpty();
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.json(messageResponse("", result.array()).MISSING_FIELD);
        } else {
            AgencyPackage.sum((err, totalData) => {
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(totalData.length == 0){
                        res.json(messageResponse(null, null).NOT_FOUND);
                    }else{
                        AgencyPackage.getAll(req.query.skip, req.query.limit, (err, agencyPackageInfo) => {
                            if(err){
                                res.json(messageResponse(err).QUERY_ERROR);
                            }else{
                                let agencyPackages = _.groupBy(agencyPackageInfo, function(agencyPackage) { 
                                    return agencyPackage.agency.full_name}
                                );
                                let newArr = [];
                                for(key in agencyPackages) {
                                    if(agencyPackages.hasOwnProperty(key)) {
                                        agencyPackages[key].forEach(function(element){
                                            newArr.push(element);
                                        })
                                    }
                                }
                                res.json(messageResponse(null, newArr, totalData).FOUND_DATA);
                            }
                        });
                    }
                }
            });
        }
    }).catch(function(rea){
        console.log(rea);
    });
}

exports.getByDataSearchSkipLimit = (req, res) => {
	req.check("limit", messageValidate("limit")).notEmpty();
    req.check("skip", messageValidate("skip")).notEmpty();
    req.check("agencyPackageName", messageValidate("agencyPackageName")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let dataSearch = decodeURI(req.query.agencyPackageName);
            AgencyPackage.takeDataSearchSkipLimit(req.query.skip, req.query.limit, dataSearch, (err, dataList)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{                    
                    AgencyPackage.sumQuery(dataSearch, (err, data)=>{
                        if(err){
                            res.json(messageResponse(err).QUERY_ERROR);
                        }else{
                            res.json(messageResponse(null, dataList, data).FOUND_DATA);
                        }
                    });
                }
            });
    	}
    });
}

exports.getAgencyPackageById = (req, res) => {
    req.check("agency_package_id", messageValidate("agency_package_id")).notEmpty();
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.json(messageResponse("", result.array()).MISSING_FIELD);
        } else {
            AgencyPackage.takeAgencyPackageById(req.query.agency_package_id, (err, agencyPackage) => {
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(!agencyPackage){
                        return res.json(messageResponse(null, null).NOT_FOUND);
                    }else{
                        res.json(messageResponse(null, agencyPackage, null).FOUND_DATA);                       
                    }
                }
            });
        }
    }).catch(function(rea){
        console.log(rea);
    });
}
function calCycle (packagePrice, cycle){
    return packagePrice - (packagePrice * cycle / 100);
}
exports.update = (req, res) =>{
    req.check("id", messageValidate("id")).notEmpty();
    req.check("agency_package_name", messageValidate("agency_package_name")).notEmpty();
    req.check("agency", messageValidate("agency")).notEmpty();
    req.check("package", messageValidate("package")).notEmpty();
    req.check("cycle", messageValidate("cycle")).notEmpty().isFloat();
    req.check("agency_package_status", messageValidate("agency_package_status")).notEmpty();
    req.check("user_update", messageValidate("user_update")).notEmpty();
    
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let id = req.body.id;
            let agencyId = req.body.agency;
            let packageId = req.body.package;
            AgencyPackage.takeAgencyPackageByIdAgencyAndPackage(id, agencyId, packageId, (err, agencyPackage)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(agencyPackage){
                        res.json(messageResponse("Đại lý và gói", null).EXIST_OBJECT);
                    }else {
                        Package.takePackageById(packageId, (err, packageInfo) => {
                            if(err){
                                res.json(messageResponse(err).QUERY_ERROR);
                            }else{
                                if(!packageInfo){
                                    res.json(messageResponse("Gói", null).NOT_EXIST_OBJECT);
                                }else {
                                    let packagePrice = packageInfo.package_price;
                                    let cycle = req.body.cycle;
                                    let now = new Date();
                                    let agencyPackage = {
                                        "agency_package_name": req.body.agency_package_name,
                                        "agency": agencyId,
                                        "package": packageId,
                                        "cycle": parseFloat(req.body.cycle),
                                        "price_after_cycle": calCycle(packagePrice, cycle),
                                        "agency_package_status": req.body.agency_package_status,
                                        "day_update_date": now,                        
                                        "day_update_string": moment(now).format(constants.FORMAT_FULL_DATE),
                                        "user_update": req.body.user_update
                                    };
                                    AgencyPackage.updateAgencyPackageById(id, agencyPackage, (err, data) => {
                                        if(err){
                                            res.json(messageResponse(err).QUERY_ERROR);
                                        }else{
                                            res.json(messageResponse("đại lý và gói", data).UPDATE_SUCCESSFULLY);   
                                        }
                                    });
                                }
                            }
                        });
                        
                    }
                }
            });
        }
    });
}

exports.remove = (req, res) =>{
    req.check("agency_package", messageValidate("agency_package")).notEmpty();
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let agencyPackageId = req.query.agency_package;
            GiftCode.getAgencyPackage(agencyPackageId, (err, giftCode) => {
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else {
                    if(giftCode){
                        res.json(messageResponse("Không thể xóa vì có tồn tại giftcode", null).CUSTOM_MESSAGE);
                    }else {
                        AgencyPackage.deleteId(agencyPackageId,(err)=>{
                            if(err){
                                res.json(messageResponse(err).QUERY_ERROR);
                            }else { 
                                res.json(messageResponse(null, null).DELETE_SUCCESSFULLY);
                            }
                        })
                        
                    }
                }
            });
        }
    });
}